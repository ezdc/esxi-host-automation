
Write-Output "Conenecting to VC Host - 172.16.0.1 ..."
Connect-ViServer 172.16.0.1 -User root -Password "vmware1!"
Write-Output "Powering off the vCenter now ..."
Stop-VMGuest -VM "SQL"
sleep 2.0
Stop-VMGuest -VM "VC"
$Time = (Get-Date).TimeofDay
do {
  # Wait for the VMs to be Shutdown cleanly
  sleep 2.0
  $timeleft = $waittime - ($Newtime.seconds)
  Write "Waiting for shutdown of VC VM's or until $timeleft seconds"
  $Newtime = (Get-Date).TimeofDay   - $Time
} until ((@(Get-VM -Name "VC" | Where-Object {$_.Name -eq "VC" -or $_.Name -eq "SQL" -and $_.PowerState -eq "PoweredOn"}).Count) -eq 0 -or ($Newtime).Seconds -ge $waittime)
Write-Output "vCenter powered off."
Disconnect-VIServer 172.16.0.1 -Confirm:$false

sleep 5
Write-Output "Entering Maintenance Mode ..."
foreach ($host in $allHosts) {
	Write-Output $host + "..."
	Connect-ViServer $host -User root -Password "vmware1!"
	$vmhost= Get-VMHost $host
	$spec = new-object VMware.Vim.HostMaintenanceSpec
	$spec.VsanMode = new-object VMware.Vim.VsanHostDecommissionMode
	$spec.VsanMode.ObjectAction = "noAction"
	$vmhost.ExtensionData.EnterMaintenanceMode(0, $false, $spec)
	Disconnect-VIServer $host -Confirm:$false
}
