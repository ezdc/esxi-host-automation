
$allHosts = @("172.16.0.1", "172.16.0.2", "172.16.0.3")

Connect-ViServer 172.16.1.3 -User "administrator@vsphere.local" -Password "vmware1!"

# Get All the ESX Hosts
$AllVM = Get-VM | Where-Object {$_.Name -ne "VC" -and $_.Name -ne "SQL" -and $_.PowerState -eq "PoweredOn"}

Write-Output "All VM's = " + $AllVM

if (($AllVM.Count) -ne 0) {
	Write-Output "VM's to be powered off ..."
	# For each of the VMs on the ESX hosts
	Foreach ($VM in $AllVM){
		# Shutdown the guest cleanly
		Write-Output "Powering off $VM.Name ..."
		$VM | Stop-VMGuest -Confirm:$false
	}
}

# Set the amount of time to wait before assuming the remaining powered on guests are stuck
$waittime = 200 #Seconds
$Time = (Get-Date).TimeofDay
if ((Get-VM | Where-Object {$_.Name -ne "VC" -and $_.Name -ne "SQL" -and $_.PowerState -eq "PoweredOn"}).Count -ne 0) {
	Write-Output "VM's to be powered off ..."
	do {
	  # Wait for the VMs to be Shutdown cleanly
	  sleep 2.0
	  $timeleft = $waittime - ($Newtime.seconds)
	  $numvms = (Get-VM | Where-Object {$_.Name -ne "VC" -and $_.Name -ne "SQL" -and $_.PowerState -eq "PoweredOn"}).Count
	  Write "Waiting for shutdown of $numvms VMs or until $timeleft seconds"
	  $Newtime = (Get-Date).TimeofDay  - $Time
	} until ((Get-VM | Where-Object {$_.Name -ne "VC" -and $_.Name -ne "SQL" -and $_.PowerState -eq "PoweredOn"}).Count -eq 0 -or ($Newtime).Seconds -ge $waittime)
}
Write-Output "Disconnecting from the VC ..."
Disconnect-VIServer 172.16.1.3 -Confirm:$false
