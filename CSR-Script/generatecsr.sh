#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Script Requires a mandatory parameter -t|--inputfile and an optional -d|--domain"
    exit 1
fi

if  [ $# -lt 10 ] ; then
    echo "Please check the number of parameters provided"
    exit 1
fi

# read the options
TEMP=`getopt -o d:t:o:c:p: --long domain:,inputfile:,outputdir:,csrinputfile:,opensslpath: -n 'test.sh' -- "$@"`
eval set -- "$TEMP"

# extract options and their arguments into variables.
while true ; do
    case "$1" in
        -d|--domain)
            case "$2" in
                "") shift 2 ;;
                *) DOMAIN=$2 ; shift 2 ;;
            esac ;;
        -t|--inputfile)
    case "$2" in
                "") shift 2 ;;
                *) INFILE=$2 ; shift 2 ;;
           esac ;;
	-o|--outdir)
     case "$2" in
                "") shift 2 ;;
                *) OUTDIR=$2 ; shift 2 ;;
           esac ;;
 	-c|--csrinputfile)
     case "$2" in
                "") shift 2 ;;
                *) CSRFILE=$2 ; shift 2 ;;
           esac ;;
 	-p|--opensslpath)
     case "$2" in
                "") shift 2 ;;
                *) SSLCNFFILE=$2 ; shift 2 ;;
           esac ;;
	--) shift; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

if [ ! -f $INFILE ] ; then
    echo "File $INFILE doesnot exist"
    exit 1
fi

if [ ! -d $OUTDIR ] ; then
    echo "Directory $OUTDIR doesnot exist"
    exit 1
fi

if [ ! -f $CSRFILE ] ; then
    echo "File $CSRFILE doesnot exist"
    exit 1
fi

if [ ! -f $SSLCNFFILE ] ; then
    echo "File $SSLCNFFILE doesnot exist"
    exit 1
fi

IFS='='
while read key value
do
	if [[ $key =~ ^C$ ]]; then
	 Country=$value
	fi
	if [[ $key =~ ^ST$ ]]; then
         State=$value
        fi
	if [[ $key =~ ^L$ ]]; then
         Location=$value
        fi
	if [[ $key =~ ^O$ ]]; then
         Org=$value
        fi
	if [[ $key =~ ^OU$ ]]; then
         Orgunit=$value
        fi
	if [[ $key =~ ^emailAddress$ ]]; then
         Email=$value
        fi
done < $CSRFILE 
IFS=','
while read hostname ipad
do
HOSTNAME=${hostname}'.'${DOMAIN}
  openssl req -nodes -newkey rsa:2048 -keyout ./$OUTDIR/$ipad.key -out ./$OUTDIR/$ipad.csr -subj "/C=$Country/ST=$State/L=$Location/O=$Org/OU=$Orgunit/CN=$HOSTNAME/emailAddress=$Email" -reqexts v3_req  -config <(cat $SSLCNFFILE | sed  "s/IP.1 = 1.1.1.1/IP.1 = $ipad/; s/DNS.1 = replace/DNS.1 = $HOSTNAME/")
done < $INFILE	
