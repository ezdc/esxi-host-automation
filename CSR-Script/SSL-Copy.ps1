$ESXIHost=Read-Host "Enter the HostName"
$ESXIUser=Read-Host "Enter the Host Admin Name"
$ESXIPassword=Read-Host "Enter the Host Admin Password"
$CrtFilePath=Read-Host "Enter the .Crt File path ex(E:\rui.crt)"
$KeyFilePath=Read-Host "Enter the .Key File path ex(E:\rui.crt)"

$GetSSH= Get-Module Posh-SSH
if($getSSH -eq $null){
	Find-Module Posh-SSH | Install-Module
}
if($getSSH -ne $null){
	$password=$ESXIPassword | ConvertTo-SecureString -AsPlainText -Force
	$cred=New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $ESXIUser,$Password
	$Session=Posh-SSH\New-SSHSession -ComputerName $ESXIHost -Credential $cred
	if($Session -ne $null){
		Write-output "[Host-$ESXIHost] Connected"
		$GetSession=Get-SSHSession
		$moveCRT=Invoke-SSHCommand -SSHSession $GetSession -Command "cd /etc/vmware/ssl ; mv rui.crt rui.crt.bak"
		Write-output "[Host-$ESXIHost] Renamed rui.crt File"
		$moveKey=Invoke-SSHCommand -SSHSession $GetSession -Command "cd /etc/vmware/ssl ; mv rui.key rui.Key.bak"
		Write-output "[Host-$ESXIHost] Renamed rui.Key File"
		Set-SCPFile -LocalFile $CrtFilePath -RemotePath /etc/vmware/ssl/ -ComputerName $ESXIHost -Credential $cred
		Write-output "[Host-$ESXIHost] Copy rui.Crt File from Local Machine and past to $ESXIHost-Completed"
		Set-SCPFile -LocalFile $KeyFilePath -RemotePath /etc/vmware/ssl/ -ComputerName $ESXIHost -Credential $cred
		Write-output "[Host-$ESXIHost] Copy rui.Key File from Local Machine and past to $ESXIHost-Completed"
		$moveCRT=Invoke-SSHCommand -SSHSession $GetSession -Command "services.sh restart"
		Write-output "[Host-$ESXIHost] Restarts all the Services"
		$remove=Remove-SSHSession $GetSession
		Write-output "[Host-$ESXIHost] Disconnected"
	}
}