###Initial Validations###
[CmdletBinding(SupportsShouldProcess=$true)]
param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$FilePath =  $(throw "-CSV parameter is required. This is the input csv file." )
	)

	if (-not ($FilePath | Test-Path)) {
    # file with path $path doesn't exist
	Write-Output "$FilePath doesnot exist. Kindly retry with correct file"
	exit
	}
	

#$vCenterServer ="Consultant-VC.sg.local"
$vCenterServer = $null
$Createdatacenter = $null
$Create_cluster = $null

$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
$Error.Clear()
if ($vmsnapin -eq $null)
{
Add-PSSnapin VMware.VimAutomation.Core
if ($error.Count -eq 0)
{
write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
}
else
{
write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
Exit
}
}
else
{
Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
}

$Error.Clear()
#connect vCenter from parameter

$Value = 0

try {

		
		$CSV = Import-CSV $FilePath
		ForEach ($Inputcsv in $CSV) {
		
			if($Inputcsv.Config -eq "vCenter"){
				$vCenterServer = $Inputcsv.Value
				Connect-VIServer -Server $vCenterServer -ErrorAction SilentlyContinue | Out-Null
			}
			Elseif ($Inputcsv.Config -eq "Datacenter Name" -AND $Value -eq 0 ){
				
				$Datacenter = $Inputcsv.Value
				### Check Datacenter name 
				$ValidDatacenter = Get-Datacenter -Name $Datacenter -ErrorAction SilentlyContinue
				
				if($ValidDatacenter -eq $null){
					### Create Datacenter name
					
					$Createdatacenter = New-Datacenter -Location (Get-Folder -NoRecursion) -Name $Datacenter
					$Value = 1
				}
				else
				{
					Write-Host "Datacenter Name is $Datacenter Already Exit"
					$Createdatacenter = $Inputcsv.Value
					$Value = 1
				}
			}
			Elseif  ($Inputcsv.Config -eq "cluster name" -AND $Value -eq 1){
					#Write-Host $Inputcsv.Value
					$Clustername = $Inputcsv.Value.Split(',')
					Foreach ($Cluster in $Clustername)
					{
						
						$ValidCluster = Get-Cluster -Name $Cluster -ErrorAction SilentlyContinue
						if($ValidCluster -eq $null){
							$CreateCluster = New-Cluster -Location $Createdatacenter -Name $Cluster -DRSEnabled 
						}
						else{ Write-Host "Cluster Name Already Exit in $Createdatacenter"	}
						
					}
					$Value = 2
			}
			Elseif  ($Inputcsv.Config -eq "Folder name" -AND $Value -eq 2){
							$Foldername = $Inputcsv.Value.Split(',')
							Foreach ($Folder in $Foldername){
								
								$ValidFolder = Get-Folder -Name $Folder -Location $Createdatacenter -ErrorAction SilentlyContinue
								if($ValidFolder -eq $null){
									### Create Folder name
									New-Folder -Name $Folder -Location $Createdatacenter
								}
								else
								{
									Write-Host "Folder Already Exit in $Createdatacenter"
								}
								
								
							}
					$Value = 0	
			}
				
		}
	}

catch {
			Write-Host $_.Exception -ExitGracefully $True
}