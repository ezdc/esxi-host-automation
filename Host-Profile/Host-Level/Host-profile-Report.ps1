<#
.SYNOPSIS
	Getting ESXI Security Profile Settings
.DESCRIPTION
	Getting ESXI Security Profile Settings and Export into CSV
.EXAMPLE
	.\Host-Profile-Report.ps1 .\Host-profile.CSV
	Enter the option:1
	Enter the Host Settings CSV File Output Path:D\xxxx\Host-Report.CSV
	Enter the VM Settings CSV File Output Path:D\xxxx\VM-Report.CSV
	
	Host-Profile.ps1.CSV- Host Information deatils
.NOTES
	File Name : Host-profile.ps1
	Author    : Raj Srinivasan
	Requires  : PowerCLI 
	Version   : 1.0
#>
###################[Inputfile Validation]####################################
 	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSVFile =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($InputCSVFile | Test-Path)) {
		# file with path $path doesn't exist
		Write-Output "$InputCSVFile doesnot exist. Kindly retry with correct file" -foregroundcolor red
		Write-Host
		exit
	}
	$Global:Action=$null
	$Global:VMCSV=$null
	$Global:HostCSV=$null
###################[Declaring Input File ]###########################
	clear
	Write-Host "1.Secuity Profile 1 `n2.Secuity Profile 2`n3.Secuity Profile 3 "
    $Global:Action = Read-Host "Enter the option "
	$Global:HostCSV=Read-Host "Enter the Host Settings CSV File Output Path"
	$Global:VMCSV=Read-Host "Enter the VM Settings CSV File Output Path"
	$InputCSV = Import-Csv -Path $InputCSVFile
	#$ErrorActionPreference = "SilentlyContinue"
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}

###################[Main Function]###########################
	$Results = @()
	$ResultsVM = @()
	$ResultsVM1 = @()
	$ResultsVM2 = @()
	$ResultsVMs=@()
	
	Disconnect-VIServer * -Confirm:$false
	foreach($Hostvalue in $InputCSV){
		$HostIP=$Hostvalue.HostIP
		$HostUserName=$Hostvalue.HostUserName
		$HostUserPassword=$Hostvalue.HostUserPassword
		$resultsHost = Connect-VIServer -Server $HostIP -user $HostUserName -Password $HostUserPassword -WarningAction silentlycontinue
		If ($resultsHost -ne $null){
			Write-Output "*****************************[$HostIP]**********************************"
			Write-Output "[HOST-$HostIP] Connected"
			$HostServer=Get-VMHost -Name $HostIP -ErrorAction silentlycontinue
			Write-Output "[Host-$HostIP] Getting Security Profile Values"	
			if($Global:Action -eq 1){
				$ESXCli = Get-EsxCli -VMHost $HostIP -WarningAction silentlycontinue
				$Acceptancelevel=$ESXCli.software.acceptance.get()
				$VMS=Get-VMHost $HostIP | Get-VM 
				if($VMS -ne $null){
					foreach($vm in $VMS){
						Write-Output "[Host-$HostIP] [VM-$vm] Getting Security Profile Values"
						$hgfs=Get-AdvancedSetting -Name "isolation.tools.hgfsServerSet.disable" -Entity $vm
						$ghi=Get-AdvancedSetting -Name "isolation.tools.ghi.autologon.disable" -Entity $vm
						$biosbbs=Get-AdvancedSetting -Name "isolation.bios.bbs.disable" -Entity $vm
						$getCreds=Get-AdvancedSetting -Name "isolation.tools.getCreds.disable" -Entity $vm
						$launchmenu=Get-AdvancedSetting -Name "isolation.tools.ghi.launchmenu.change" -Entity $vm
						$memsfss=Get-AdvancedSetting -Name "isolation.tools.memSchedFakeSampleStats.disable" -Entity $vm
						$protocolhandler=Get-AdvancedSetting -Name "isolation.tools.ghi.protocolhandler.info.disable" -Entity $vm
						$shellaction=Get-AdvancedSetting -Name "isolation.ghi.host.shellAction.disable" -Entity $vm
						$toporequest=Get-AdvancedSetting -Name "isolation.tools.dispTopoRequest.disable" -Entity $vm
						$trashfolderstate=Get-AdvancedSetting -Name "isolation.tools.trashFolderState.disable" -Entity $vm
						$trayicon=Get-AdvancedSetting -Name "isolation.tools.ghi.trayicon.disable" -Entity $vm
						$unity=Get-AdvancedSetting -Name "isolation.tools.unity.disable" -Entity $vm
						$interlock=Get-AdvancedSetting -Name "isolation.tools.unityInterlockOperation.disable" -Entity $vm
						$unitypush=Get-AdvancedSetting -Name "isolation.tools.unity.push.update.disable" -Entity $vm
						$taskbar=Get-AdvancedSetting -Name "isolation.tools.unity.taskbar.disable" -Entity $vm
						$unityactive=Get-AdvancedSetting -Name "isolation.tools.unityActive.disable" -Entity $vm
						$windowcontents=Get-AdvancedSetting -Name "isolation.tools.unity.windowContents.disable" -Entity $vm
						$versionget=Get-AdvancedSetting -Name "isolation.tools.vmxDnDVersionGet.disable" -Entity $vm
						$versionset=Get-AdvancedSetting -Name "isolation.tools.guestDnDVersionSet.disable" -Entity $vm
						$vix=Get-AdvancedSetting -Name "isolation.tools.vixMessage.disable" -Entity $vm 
						$PropertiesVM = @{
							Host=$HostIP
							VM=$vm.Name
							isolationtoolshgfsServerSet=$hgfs.Value
							isolationtoolsGhiAutologon=$ghi.Value
							isolationtoolsBiosBbs=$biosbbs.Value
							isolationtoolsgetCreds=$getCreds.Value
							isolationtoolGhisLaunchmenu=$launchmenu.Value
							isolationtoolSampleStats=$memsfss.Value
							isolationGhiProtocolhandler=$protocolhandler.Value
							isolationGhiHostShellAction=$shellaction.Value
							isolationtoolsDispTopoRequest=$toporequest.Value
							isolationtoolsTrashFolderState=$trashfolderstate.Value
							isolationtoolsGhiTrayicon=$trayicon.Value
							isolationtoolsUnity=$unity.Value
							isolationtoolsUnityInterlockOperation=$interlock.Value
							isolationtoolsUnityPushUpdate=$unitypush.Value
							isolationtoolsUnityTaskBar=$taskbar.Value
							isolationtoolsUnityActive=$unityactive.Value
							isolationtoolsUnityWindowContents=$windowcontents.Value
							isolationtoolsVmxDnDVersionGet=$versionget.Value
							isolationtoolsGuestDnDVersionSet=$versionset.Value
							isolationtoolsVixMessage=$vix.Value
						}
						$ResultsVM1 += New-Object PSObject -Property $PropertiesVM	
					}
				}		
			}
				
			if($Global:Action -eq 2){
				$ESXCli = Get-EsxCli -VMHost $HostIP -WarningAction silentlycontinue
				$Acceptancelevel=$ESXCli.software.acceptance.get()
			}
				
			if($Global:Action -eq 3){
				$ESXCli = Get-EsxCli -VMHost $HostIP -WarningAction silentlycontinue
				$Acceptancelevel=$ESXCli.software.acceptance.get()
			}
				
			if(($Global:Action -eq 1) -or ($Global:Action -eq 2)){
				$VMS=Get-VMHost $HostIP | Get-VM 
				Function Get-ParallelPort { 
					Param ( 
						[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] 
						$VM 
					) 
					Process { 
						Foreach ($VMachine in $VM) { 
							Foreach ($Device in $VMachine.ExtensionData.Config.Hardware.Device) { 
								If ($Device.gettype().Name -eq "VirtualParallelPort"){ 
									$Details = New-Object PsObject 
									$Details | Add-Member Noteproperty VM -Value $VMachine 
									$Details | Add-Member Noteproperty Name -Value $Device.DeviceInfo.Label 
									If ($Device.Backing.FileName) { $Details | Add-Member Noteproperty Filename -Value $Device.Backing.FileName } 
									If ($Device.Backing.Datastore) { $Details | Add-Member Noteproperty Datastore -Value $Device.Backing.Datastore } 
									If ($Device.Backing.DeviceName) { $Details | Add-Member Noteproperty DeviceName -Value $Device.Backing.DeviceName } 
									$Details | Add-Member Noteproperty Connected -Value $Device.Connectable.Connected 
									$Details | Add-Member Noteproperty StartConnected -Value $Device.Connectable.StartConnected 
									$Details 
								} 
							} 
						}
					} 
				} 
				Function Get-SerialPort { 
					Param ( 
						[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] 
						$VM 
					) 
					Process { 
						Foreach ($VMachine in $VM) { 
							Foreach ($Device in $VMachine.ExtensionData.Config.Hardware.Device) { 
								If ($Device.gettype().Name -eq "VirtualSerialPort"){ 
									$Details = New-Object PsObject 
									$Details | Add-Member Noteproperty VM -Value $VMachine 
									$Details | Add-Member Noteproperty Name -Value $Device.DeviceInfo.Label 
									If ($Device.Backing.FileName) { $Details | Add-Member Noteproperty Filename -Value $Device.Backing.FileName } 
									If ($Device.Backing.Datastore) { $Details | Add-Member Noteproperty Datastore -Value $Device.Backing.Datastore } 
									If ($Device.Backing.DeviceName) { $Details | Add-Member Noteproperty DeviceName -Value $Device.Backing.DeviceName } 
									$Details | Add-Member Noteproperty Connected -Value $Device.Connectable.Connected 
									$Details | Add-Member Noteproperty StartConnected -Value $Device.Connectable.StartConnected 
									$Details 
								} 
							} 
						} 
					} 
				}
				if($VMS -ne $null){
					foreach($vm in $VMS){
						Write-Output "[Host-$HostIP] [VM-$vm] Getting Security Profile Values"
						$NonPersistents=Get-VM $vm | Get-HardDisk | where {$_.Persistence -ne "Persistent"}
						$AutoInstall=Get-AdvancedSetting -Name "isolation.tools.autoInstall.disable" -Entity $vm
						$floppys=Get-VM $vm | Get-FloppyDrive
						$ParallelPorts=Get-VM $vm| Get-ParallelPort
						$SerialPorts=Get-VM $vm| Get-SerialPort
						$RestrictHost=Get-AdvancedSetting -Name "tools.guestlib.enableHostInfo" -Entity $vm
						$PropertiesVM = @{
							Host=$HostIP
							VM=$vm.Name
							NonPersistentsDisk=$NonPersistents
							AutoInstallVMTools=$AutoInstall.value
							ParallelPorts=$ParallelPorts
							SerialPorts=$SerialPorts
							floppy=$floppys
						}
						$ResultsVM2 += New-Object PSObject -Property $PropertiesVM	
					}
				}	
			}	
			
			if(($Global:Action -eq 1) -or ($Global:Action -eq 2) -or ($Global:Action -eq 3)){
				#$nfc=Get-AdvancedSetting -Entity $global:vCenter -Name "Config.nfc.useSSL"
				$NTPServer =Get-VMHostNtpServer -VMHost $HostIP
				$DomainStatus=Get-VMHost $HostIP | Get-VMHostAuthentication
				$iscsi=Get-VMHostHba -VMHost $HostIP | Where {$_.Type -eq "Iscsi"}
				$Logdir=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Syslog.global.logDir" -WarningAction silentlycontinue 
				$snmp=Get-VMHostService -VMHost $HostIP | where{$_.key -match "snmpd"}
				$MOB=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name Config.HostAgent.plugins.solo.enableMob -WarningAction silentlycontinue 	
				$AccountTime=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Security.AccountUnlockTime" -WarningAction silentlycontinue
				$AccountLock=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Security.AccountLockFailures" -WarningAction silentlycontinue
				$DCUI=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "DCUI.Access" -WarningAction silentlycontinue
				$DCUITimeOut=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.DcuiTimeOut" -WarningAction silentlycontinue
				$Syslog=Get-VMHostSysLogServer -VMHost $HostIP
				$Secuity=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Security.PasswordQualityControl" -WarningAction silentlycontinue
				$ShelInteractive=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.ESXiShellInteractiveTimeOut" -WarningAction silentlycontinue
				$ShelTimeOut=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.ESXiShellTimeOut" -WarningAction silentlycontinue
				$Salting=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Mem.ShareForceSalting" -WarningAction silentlycontinue
				$BPDU=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Net.BlockGuestBPDU" -WarningAction silentlycontinue
				$DV=Get-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Net.DVFilterBindIpAddress" -WarningAction silentlycontinue 
				$VSS=Get-VirtualSwitch -Standard -VMHost $HostIP 
				$SecurityPolicy =$null
				foreach($vs in $VSS){
					$GetVS=Get-VirtualSwitch -Standard -Name $vs.Name -VMHost $HostIP | Get-SecurityPolicy
					$Security= $vs.Name + "-" + $GetVS.AllowPromiscuous +","+ $GetVS.ForgedTransmits +","+ $GetVS.MacChanges
					$SecurityPolicy += $Security
				}
				$NetflowValue =$null
				$dvsSecurityPolicy=$null
				$OverRidePolicy=$null
				$pgs=Get-VirtualPortGroup -VMHost $HostIP -Distributed -WarningAction silentlycontinue
				foreach($pg in $pgs){
					$getover=Get-VDPortgroup $pg.Name | Get-VDPortgroupOverridePolicy
					$GetOverPolicy=$pg.Name + "-" + $getover.BlockOverrideAllowed +","+ $getover.SecurityOverrideAllowed +","+ $getover.VlanOverrideAllowed +","+ $getover.UplinkTeamingOverrideAllowed
					$OverRidePolicy += $GetOverPolicy
					$GetDvs=Get-VDPortgroup $pg.Name | Get-VDSecurityPolicy
					$dvsSecurity= $pg.Name + "-" + $GetDvs.AllowPromiscuous +","+ $GetDvs.ForgedTransmits +","+ $GetDvs.MacChanges
					$dvsSecurityPolicy += $dvsSecurity
					$Netflow=Get-VDPortgroup $pg.Name
					$Net=$pg.Name + "-" +$Netflow.ExtensionData.Config.DefaultPortConfig.IpfixEnabled.Value
					$NetflowValue += $Net
				}
				$VMS=Get-VMHost $HostIP | Get-VM 
				if($VMS -ne $null){
					foreach($vm in $VMS){
						Write-Output "[Host-$HostIP] [VM-$vm] Getting Security Profile Values"
						$Tools=Get-AdvancedSetting -Name "isolation.tools.copy.disable" -Entity $vm
						$ToolsDND=Get-AdvancedSetting -Name "isolation.tools.dnd.disable" -Entity $vm
						$ToolsGUI=Get-AdvancedSetting -Name "isolation.tools.setGUIOptions.enable" -Entity $vm
						$ToolsPaste=Get-AdvancedSetting -Name "isolation.tools.paste.disable" -Entity $vm
						$diskShrink=Get-AdvancedSetting -Name "isolation.tools.diskShrink.disable" -Entity $vm
						$diskWiper=Get-AdvancedSetting -Name "isolation.tools.diskWiper.disable" -Entity $vm
						$sizeLimit=Get-AdvancedSetting -Name "tools.setInfo.sizeLimit" -Entity $vm
						$RemoteDisplay=Get-AdvancedSetting -Name "RemoteDisplay.vnc.enabled" -Entity $vm
						$interaction=Get-AdvancedSetting -Name "isolation.device.connectable.disable" -Entity $vm
						$DeviceEdit=Get-AdvancedSetting -Name "isolation.device.edit.disable" -Entity $vm
						$Pshare=Get-AdvancedSetting -Name "sched.mem.pshare.salt" -Entity $vm
						$ethoFillter=Get-AdvancedSetting -Name  "ethernet*.filter*.name*" -Entity $vm 
						$pciPassthru=Get-AdvancedSetting -Name "pciPassthru*.present" -Entity $vm
						
						$PropertiesVM = @{
							Host=$HostIP
							VM=$vm.Name
							isolationtoolsCopy=$Tools.Value
							isolationtoolsDND=$ToolsDND.Value
							isolationtoolsSetGUIOptions=$ToolsGUI.Value
							isolationtoolsPaste=$ToolsPaste.Value
							isolationtoolsDiskShrink=$diskShrink.Value
							isolationtoolsDiskWiper=$diskWiper.Value
							isolationdeviceConnectable=$interaction.Value
							toolssetInfosizeLimit=$sizeLimit.Value
							RemoteDisplayVNC=$RemoteDisplay.Value
							isolationdeviceEdit=$DeviceEdit.Value
							schedmempshareSalt=$Pshare.Value
							ethernetFilterName=$ethoFillter.Value
							pciPassthruPresent=$pciPassthru.Value
						}
						$ResultsVM += New-Object PSObject -Property $PropertiesVM	
					}
				}	
			}			
			
			$Properties = @{	
				Host=$HostIP
				Acceptancelevel=$Acceptancelevel
				#nfcvCenter=$nfc.Value
				NTPServer=$NTPServer -join ","
				Domain=$DomainStatus.Domain
				iscsi=$iscsi
				Logdir=$Logdir.Values -join ""
				SNMP=$snmp.Running
				MOB=$MOB.Values -join ""
				AccountTime=$AccountTime.Values -join ""
				AccountLock=$AccountLock.Values -join ""
				DCUIUsers=$DCUI.Values -join ""
				DCUITimeOut=$DCUITimeOut.Values -join ""
				SyslogServer=$Syslog.Host
				PasswordQualityControl=$Secuity.Values -join ""
				ESXiShellInteractiveTimeOut=$ShelInteractive.Values -join ""
				ESXiShellTimeOut=$ShelTimeOut.Values -join ""
				ShareForceSalting=$Salting.Values -join ""
				BlockGuestBPDU=$BPDU.Values -join ""
				DVFilterBindIpAddress=$DV.Values -join ""
				SecurityPolicy=$SecurityPolicy
				Netflow=$NetflowValue
				dvsSecurityPolicy=$dvsSecurityPolicy
				dvsPortOverRidePolicy=$OverRidePolicy		
			}	
			
			$Results += New-Object PSObject -Property $Properties
		}
		else{
			Write-Warning "[Host-$HostIP] ERROR: Unable to connect"
		}
		Disconnect-VIServer -Server $HostIP -Confirm:$false
		Write-Host
		$Results | Select-Object Host,Acceptancelevel,NTPServer,Domain,iscsi,Logdir,SNMP,MOB,AccountTime,AccountLock,DCUIUsers,DCUITimeOut,SyslogServer,PasswordQualityControl,ESXiShellInteractiveTimeOut,ESXiShellTimeOut,ShareForceSalting,BlockGuestBPDU,DVFilterBindIpAddress,SecurityPolicy,Netflow,dvsSecurityPolicy,dvsPortOverRidePolicy | Export-Csv -notypeinformation -Path $Global:HostCSV		
	}
	
	if(($ResultsVM -ne $null) -and ($ResultsVM1 -ne $null) -and ($ResultsVM2 -ne $null)){
		if($Global:Action -eq 1){
			Foreach($CSV in $ResultsVM1){
				$VM=$CSV.VM
				$PropertiesVMs = @{
					Host=$CSV.Host
					VM=$CSV.VM
					isolationtoolshgfsServerSet=$CSV.isolationtoolshgfsServerSet
					isolationtoolsGhiAutologon=$CSV.isolationtoolsGhiAutologon
					isolationtoolsBiosBbs=$CSV.isolationtoolsBiosBbs
					isolationtoolsgetCreds=$CSV.isolationtoolsgetCreds
					isolationtoolGhisLaunchmenu=$CSV.isolationtoolGhisLaunchmenu
					isolationtoolSampleStats=$CSV.isolationtoolSampleStats
					isolationGhiProtocolhandler=$CSV.isolationGhiProtocolhandler
					isolationGhiHostShellAction=$CSV.isolationGhiHostShellAction
					isolationtoolsDispTopoRequest=$CSV.isolationtoolsDispTopoRequest
					isolationtoolsTrashFolderState=$CSV.isolationtoolsTrashFolderState
					isolationtoolsGhiTrayicon=$CSV.isolationtoolsGhiTrayicon
					isolationtoolsUnity=$CSV.isolationtoolsUnity
					isolationtoolsUnityInterlockOperation=$CSV.isolationtoolsUnityInterlockOperation
					isolationtoolsUnityPushUpdate=$CSV.isolationtoolsUnityPushUpdate
					isolationtoolsUnityTaskBar=$CSV.isolationtoolsUnityTaskBar
					isolationtoolsUnityActive=$CSV.isolationtoolsUnityActive
					isolationtoolsUnityWindowContents=$CSV.isolationtoolsUnityWindowContents
					isolationtoolsVmxDnDVersionGet=$CSV.isolationtoolsVmxDnDVersionGet
					isolationtoolsGuestDnDVersionSet=$CSV.isolationtoolsGuestDnDVersionSet
					isolationtoolsVixMessage=$CSV.isolationtoolsVixMessage
					NonPersistentsDisk=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty NonPersistentsDisk
					AutoInstallVMTools=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty AutoInstallVMTools
					ParallelPorts=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty ParallelPorts
					SerialPorts=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty SerialPorts
					floppy=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty floppy
					isolationtoolsDND=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty isolationtoolsDND	
					isolationtoolsCopy=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty isolationtoolsCopy
					isolationtoolsSetGUIOptions=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty isolationtoolsSetGUIOptions
					isolationtoolsPaste=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty isolationtoolsPaste
					isolationtoolsDiskShrink=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty isolationtoolsDiskShrink
					isolationtoolsDiskWiper=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty isolationtoolsDiskWiper
					isolationdeviceConnectable=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty isolationdeviceConnectable
					toolssetInfosizeLimit=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty toolssetInfosizeLimit
					RemoteDisplayVNC=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty RemoteDisplayVNC
					isolationdeviceEdit=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty isolationdeviceEdit
					schedmempshareSalt=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty schedmempshareSalt
					ethernetFilterName=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty ethernetFilterName
					pciPassthruPresent=$ResultsVM | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty pciPassthruPresent	
				}			
				$ResultsVMs += New-Object PSObject -Property $PropertiesVMs					
			}
			$ResultsVMs  | Select-Object Host,VM,isolationtoolsCopy,isolationtoolsDND,isolationtoolsSetGUIOptions,isolationtoolsPaste,isolationtoolsDiskShrink,isolationtoolsDiskWiper,isolationdeviceConnectable,toolssetInfosizeLimit,RemoteDisplayVNC,isolationdeviceEdit,schedmempshareSalt,ethernetFilterName,pciPassthruPresent,NonPersistentsDisk,AutoInstallVMTools,ParallelPorts,SerialPorts,floppy,isolationtoolshgfsServerSet,isolationtoolsGhiAutologon,isolationtoolsBiosBbs,isolationtoolsgetCreds,isolationtoolGhisLaunchmenu,isolationtoolSampleStats,isolationGhiProtocolhandler,isolationGhiHostShellAction,isolationtoolsDispTopoRequest,isolationtoolsTrashFolderState,isolationtoolsGhiTrayicon,isolationtoolsUnity,isolationtoolsUnityInterlockOperation,isolationtoolsUnityPushUpdate,isolationtoolsUnityTaskBar,isolationtoolsUnityActive,isolationtoolsUnityWindowContents,isolationtoolsVmxDnDVersionGet,isolationtoolsGuestDnDVersionSet,isolationtoolsVixMessage | Export-Csv -notypeinformation -Path $Global:VMCSV
		}
	}
		
	if(($ResultsVM -ne $null) -And ($ResultsVM2 -ne $null)) {
		if($Global:Action -eq 2){
			Foreach($CSV in $ResultsVM){
				$VM=$CSV.VM
				$PropertiesVMs = @{
					Host=$CSV.Host
					VM=$CSV.VM	
					isolationtoolsCopy=$CSV.isolationtoolsCopy
					isolationtoolsDND=$CSV.isolationtoolsDND
					isolationtoolsSetGUIOptions=$CSV.isolationtoolsSetGUIOptions
					isolationtoolsPaste=$CSV.isolationtoolsPaste
					isolationtoolsDiskShrink=$CSV.isolationtoolsDiskShrink
					isolationtoolsDiskWiper=$CSV.isolationtoolsDiskWiper
					isolationdeviceConnectable=$CSV.isolationdeviceConnectable
					toolssetInfosizeLimit=$CSV.toolssetInfosizeLimit
					RemoteDisplayVNC=$CSV.RemoteDisplayVNC
					isolationdeviceEdit=$CSV.isolationdeviceEdit
					schedmempshareSalt=$CSV.schedmempshareSalt
					ethernetFilterName=$CSV.ethernetFilterName
					pciPassthruPresent=$CSV.pciPassthruPresent
					NonPersistentsDisk=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty NonPersistentsDisk
					AutoInstallVMTools=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty AutoInstallVMTools
					ParallelPorts=$ResultsVM2| Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty ParallelPorts
					SerialPorts=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty SerialPorts
					floppy=$ResultsVM2 | Where-Object {$_.VM -eq $VM} | Select-Object -ExpandProperty floppy
				}
				$ResultsVMs += New-Object PSObject -Property $PropertiesVMs	
			}
			$ResultsVMs  | Select-Object Host,VM,isolationtoolsCopy,isolationtoolsDND,isolationtoolsSetGUIOptions,isolationtoolsPaste,isolationtoolsDiskShrink,isolationtoolsDiskWiper,isolationdeviceConnectable,toolssetInfosizeLimit,RemoteDisplayVNC,isolationdeviceEdit,schedmempshareSalt,ethernetFilterName,pciPassthruPresent,NonPersistentsDisk,AutoInstallVMTools,ParallelPorts,SerialPorts,floppy | Export-Csv -notypeinformation -Path $Global:VMCSV
		}
	}
		
	if($ResultsVM -ne $null){
		if($Global:Action -eq 3){
			$ResultsVM | Select-Object Host,VM,isolationtoolsCopy,isolationtoolsDND,isolationtoolsSetGUIOptions,isolationtoolsPaste,isolationtoolsDiskShrink,isolationtoolsDiskWiper,isolationdeviceConnectable,toolssetInfosizeLimit,RemoteDisplayVNC,isolationdeviceEdit,schedmempshareSalt,ethernetFilterName,pciPassthruPresent | Export-Csv -notypeinformation -Path $Global:VMCSV
		}
	}
	

Write-Output "*****************************[Completed]***************************************"