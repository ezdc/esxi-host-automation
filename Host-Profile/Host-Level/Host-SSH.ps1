<#
.SYNOPSIS
	Start and stop SSH Services
.DESCRIPTION
	Start and stop SSH Services in Host.
.EXAMPLE
	.\Host-SSH.ps1 .\Host-SSH.CSV
	Host-SSH.ps1.CSV -Host Information deatils
.NOTES
	FileName  : Host-SSH.ps1
	Author    : Raj Srinivasan
	Requires  : PowerCLI 
	Version   : 1.0
#>
###################[Inputfile Validation]####################################
[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSVFile =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($InputCSVFile | Test-Path)) {
		# file with path $path doesn't exist
		Write-Output "$InputCSVFile doesnot exist. Kindly retry with correct file" -foregroundcolor red
		Write-Host
		exit
	}
##################[Declaring Input File ]###########################
	clear
	$InputCSV = Import-Csv -Path $InputCSVFile
	#$ErrorActionPreference = "SilentlyContinue"
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}

###################[Main Function]###########################	
	Disconnect-VIServer * -Confirm:$false
	foreach($Hostvalue in $InputCSV){
		$HostIP=$Hostvalue.HostIP
		$HostUserName=$Hostvalue.HostUserName
		$HostUserPassword=$Hostvalue.HostUserPassword
		$SSH=$Hostvalue.SSH
		If($SSH -ne ""){
			$resultsHost = Connect-VIServer -Server $HostIP -user $HostUserName -Password $HostUserPassword -WarningAction silentlycontinue
			If ($resultsHost -ne $null){
				Write-Output "*****************************[$HostIP]**********************************"
				Write-Output "[HOST-$HostIP] Connected"
				$Status=Get-VMHostService -VMHost $HostIP | Where { $_.Key -eq "TSM-SSH" }
				
					if($SSH -eq "Start"){
						if($Status.Running -ne $true){
							$StartSSH=Get-VMHostService -VMHost $HostIP | Where { $_.Key -eq "TSM-SSH" }  | Start-VMHostService
							Write-Output "[Host-$HostIP] SSH service started"
						}else{
							Write-Warning "[Host-$HostIP] Already SSH service started"
						}
					}
					else{
						if($Status.Running -ne $false){
							$StopSSH=Get-VMHostService -VMHost $HostIP | Where { $_.Key -eq "TSM-SSH" }  | Stop-VMHostService -Confirm:$false
							Write-Output "[Host-$HostIP] SSH service stopped"
						}else{
							Write-Warning "[Host-$HostIP] Already SSH service stopped"
						}
					}
				
			}
			else{
				Write-Warning "[Host-$HostIP] ERROR: Unable to connect"
			}
			Disconnect-VIServer -Server $HostIP -Confirm:$false
			Write-Output "[HOST-$HostIP] Disconnected"
			Write-Host
		}
		else{
				Write-Warning "[HOST-$HostIP] SSH values is Empty and not Configured"
		}
	}
	Write-Output "*****************************[Completed]**************************************"