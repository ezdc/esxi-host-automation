<#
.SYNOPSIS
	Configure ESXI Security Profile Settings
.DESCRIPTION
	Configure ESXI Security Profile Settings-Configure Host AdvancedSettings,vCenter Advanced setting and VM Advanced Settings
.EXAMPLE
	.\Host-Profile-All.ps1 .\Host-profile.CSV
	Host-Profile.ps1.CSV- Host Information deatils
.NOTES
	File Name : Host-profile-All.ps1
	Author    : Raj Srinivasan
	Requires  : PowerCLI 
	Version   : 2.0
#>
###################[Inputfile Validation]####################################
 	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSVFile =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($InputCSVFile | Test-Path)) {
		# file with path $path doesn't exist
		Write-Output "$InputCSVFile doesnot exist. Kindly retry with correct file" -foregroundcolor red
		Write-Host
		exit
	}
	$Global:Action=$null
###################[Declaring Input File ]###########################
	clear
	Write-Host "1.Secuity Profile 1 `n2.Secuity Profile 2`n3.Secuity Profile 3 "
    $Global:Action = Read-Host "Enter the option "
	$InputCSV = Import-Csv -Path $InputCSVFile
	#$ErrorActionPreference = "SilentlyContinue"
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
###################[Main Function]###########################	

	Disconnect-VIServer * -Confirm:$false
	foreach($Hostvalue in $InputCSV){
		$HostIP=$Hostvalue.HostIP
		$HostUserName=$Hostvalue.HostUserName
		$HostUserPassword=$Hostvalue.HostUserPassword
		$NTPvalue=$Hostvalue.NTPServer
		$SysLogServer=$Hostvalue.SysLogServer
		$SysLogGlobal=$Hostvalue.SysLogGlobal
		$Domain=$Hostvalue.Domain
		$DomainAdmin=$Hostvalue.DomainAdmin
		$DomainAdminPwd=$Hostvalue.DomainAdminPwd
		$LocalUsers=$Hostvalue.LocalUsers
		$Acceptancelevel=$Hostvalue.Acceptancelevel
		$ChapType=$Hostvalue.ChapType
		$ChapName=$Hostvalue.ChapName
		$ChapPassword=$Hostvalue.ChapPassword
		$results = Connect-VIServer -Server $HostIP -user $HostUserName -Password $HostUserPassword -WarningAction silentlycontinue
		If ($results -ne $null){
			Write-Output "*****************************[$HostIP]**********************************"
			Write-Output "[HOST-$HostIP] Connected"
			if($Global:Action -eq 1){
				#ESXi.verify-acceptance-level-supported
				$ESXCli = Get-EsxCli -VMHost $HostIP -WarningAction silentlycontinue
				$Acceptancelevel=$ESXCli.software.acceptance.get()
				if($Acceptancelevel -ne "VMwareCertified"){
					$ESXCli.software.acceptance.Set("VMwareCertified") | out-null
					Write-Output "[HOST-$HostIP] Set Acceptancelevel-VMwareCertified"
				}
				else{
					Write-Warning "[HOST-$HostIP] Already set Acceptancelevel-VMwareCertified"
				}
				$VMS=Get-VMHost $HostIP | Get-VM 
				if($VMS -ne $null){
					foreach($vm in $VMS){
						#VM.disable-hgfs
						$hgfs=Get-AdvancedSetting -Name "isolation.tools.hgfsServerSet.disable" -Entity $vm
						if($hgfs -eq $null){
							New-AdvancedSetting -Name "isolation.tools.hgfsServerSet.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools hgfsServerSet disable"
						}
						else{
							if($hgfs.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.hgfsServerSet.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools hgfsServerSet disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools hgfsServerSet"
							}
						}
						#VM.disable-unexposed-features-autologon
						$ghi=Get-AdvancedSetting -Name "isolation.tools.ghi.autologon.disable" -Entity $vm
						if($ghi -eq $null){
							New-AdvancedSetting -Name "isolation.tools.ghi.autologon.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools ghi autologon disable"
						}
						else{
							if($ghi.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.ghi.autologon.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools ghi autologon disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools ghi autologon"
							}
						}
						#VM.disable-unexposed-features-biosbbs
						$biosbbs=Get-AdvancedSetting -Name "isolation.bios.bbs.disable" -Entity $vm
						if($biosbbs -eq $null){
							New-AdvancedSetting -Name "isolation.bios.bbs.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation bios bbs disable"
						}
						else{
							if($biosbbs.value -eq $false){
								Get-AdvancedSetting -Name "isolation.bios.bbs.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation bios bbs disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation bios bbs"
							}
						}
						#VM.disable-unexposed-features-getcreds
						$getCreds=Get-AdvancedSetting -Name "isolation.tools.getCreds.disable" -Entity $vm
						if($getCreds -eq $null){
							New-AdvancedSetting -Name "isolation.tools.getCreds.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools getCreds disable"
						}
						else{
							if($getCreds.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.getCreds.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools getCreds disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools getCreds"
							}
						}
						#VM.disable-unexposed-features-launchmenu
						$launchmenu=Get-AdvancedSetting -Name "isolation.tools.ghi.launchmenu.change" -Entity $vm
						if($launchmenu -eq $null){
							New-AdvancedSetting -Name "isolation.tools.ghi.launchmenu.change" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools ghi launchmenu disable"
						}
						else{
							if($launchmenu.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.ghi.launchmenu.change" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools ghi launchmenu disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools ghi launchmenu"
							}
						}
						#VM.disable-unexposed-features-memsfss
						$memsfss=Get-AdvancedSetting -Name "isolation.tools.memSchedFakeSampleStats.disable" -Entity $vm
						if($memsfss -eq $null){
							New-AdvancedSetting -Name "isolation.tools.memSchedFakeSampleStats.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools memSchedFakeSampleStats disable"
						}
						else{
							if($memsfss.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.memSchedFakeSampleStats.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools memSchedFakeSampleStats disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools memSchedFakeSampleStats"
							}
						}
						#VM.disable-unexposed-features-protocolhandler
						$protocolhandler=Get-AdvancedSetting -Name "isolation.tools.ghi.protocolhandler.info.disable" -Entity $vm
						if($protocolhandler -eq $null){
							New-AdvancedSetting -Name "isolation.tools.ghi.protocolhandler.info.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools ghi protocolhandler info disable"
						}
						else{
							if($protocolhandler.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.ghi.protocolhandler.info.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools  ghi protocolhandler info disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools  ghi protocolhandler info"
							}
						}
						#VM.disable-unexposed-features-shellaction
						$shellaction=Get-AdvancedSetting -Name "isolation.ghi.host.shellAction.disable" -Entity $vm
						if($shellaction -eq $null){
							New-AdvancedSetting -Name "isolation.ghi.host.shellAction.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation ghi host shellAction disable"
						}
						else{
							if($shellaction.value -eq $false){
								Get-AdvancedSetting -Name "isolation.ghi.host.shellAction.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation ghi host shellAction disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation ghi host shellAction"
							}
						}
						#VM.disable-unexposed-features-toporequest
						$toporequest=Get-AdvancedSetting -Name "isolation.tools.dispTopoRequest.disable" -Entity $vm
						if($toporequest -eq $null){
							New-AdvancedSetting -Name "isolation.tools.dispTopoRequest.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools dispTopoRequest disable"
						}
						else{
							if($toporequest.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.dispTopoRequest.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools dispTopoRequest disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools dispTopoRequest"
							}
						}
						#VM.disable-unexposed-features-trashfolderstate
						$trashfolderstate=Get-AdvancedSetting -Name "isolation.tools.trashFolderState.disable" -Entity $vm
						if($trashfolderstate -eq $null){
							New-AdvancedSetting -Name "isolation.tools.trashFolderState.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools trashFolderState disable"
						}
						else{
							if($trashfolderstate.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.trashFolderState.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools trashFolderState disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools trashFolderState"
							}
						}
						#VM.disable-unexposed-features-trayicon
						$trayicon=Get-AdvancedSetting -Name "isolation.tools.ghi.trayicon.disable" -Entity $vm
						if($trayicon -eq $null){
							New-AdvancedSetting -Name "isolation.tools.ghi.trayicon.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools ghi trayicon disable"
						}
						else{
							if($trayicon.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.ghi.trayicon.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools ghi trayicon disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools ghi trayicon"
							}
						}
						#VM.disable-unexposed-features-unity
						$unity=Get-AdvancedSetting -Name "isolation.tools.unity.disable" -Entity $vm
						if($unity -eq $null){
							New-AdvancedSetting -Name "isolation.tools.unity.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unity disable"
						}
						else{
							if($unity.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.unity.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools Unity disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools Unity"
							}
						}
						#VM.disable-unexposed-features-unity-interlock
						$interlock=Get-AdvancedSetting -Name "isolation.tools.unityInterlockOperation.disable" -Entity $vm
						if($interlock -eq $null){
							New-AdvancedSetting -Name "isolation.tools.unityInterlockOperation.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unityInterlockOperation disable"
						}
						else{
							if($interlock.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.unityInterlockOperation.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unityInterlockOperation disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools unityInterlockOperation"
							}
						}
						#VM.disable-unexposed-features-unitypush
						$unitypush=Get-AdvancedSetting -Name "isolation.tools.unity.push.update.disable" -Entity $vm
						if($unitypush -eq $null){
							New-AdvancedSetting -Name "isolation.tools.unity.push.update.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unity push update disable"
						}
						else{
							if($unitypush.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.unity.push.update.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unity push update disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools unity push update"
							}
						}
						#VM.disable-unexposed-features-unity-taskbar
						$taskbar=Get-AdvancedSetting -Name "isolation.tools.unity.taskbar.disable" -Entity $vm
						if($taskbar -eq $null){
							New-AdvancedSetting -Name "isolation.tools.unity.taskbar.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unity taskbar disable"
						}
						else{
							if($taskbar.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.unity.taskbar.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unity taskbar disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools taskbar"
							}
						}
						#VM.disable-unexposed-features-unity-unityactive
						$unityactive=Get-AdvancedSetting -Name "isolation.tools.unityActive.disable" -Entity $vm
						if($unityactive -eq $null){
							New-AdvancedSetting -Name "isolation.tools.unityActive.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unityActive disable"
						}
						else{
							if($unityactive.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.unityActive.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unityActive disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools unityActive"
							}
						}
						#VM.disable-unexposed-features-unity-windowcontents
						$windowcontents=Get-AdvancedSetting -Name "isolation.tools.unity.windowContents.disable" -Entity $vm
						if($windowcontents -eq $null){
							New-AdvancedSetting -Name "isolation.tools.unity.windowContents.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unity windowContents disable"
						}
						else{
							if($windowcontents.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.unity.windowContents.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools unity windowContents disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools unity windowContents"
							}
						}
						#VM.disable-unexposed-features-versionget 
						$versionget=Get-AdvancedSetting -Name "isolation.tools.vmxDnDVersionGet.disable" -Entity $vm
						if($versionget -eq $null){
							New-AdvancedSetting -Name "isolation.tools.vmxDnDVersionGet.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools vmxDnDVersionGet disable"
						}
						else{
							if($versionget.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.vmxDnDVersionGet.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools vmxDnDVersionGet disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools vmxDnDVersionGet"
							}
						}
						#VM.disable-unexposed-features-versionset
						$versionset=Get-AdvancedSetting -Name "isolation.tools.guestDnDVersionSet.disable" -Entity $vm
						if($versionset -eq $null){
							New-AdvancedSetting -Name "isolation.tools.guestDnDVersionSet.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools vmxDnDVersionSet disabled"
						}
						else{
							if($versionset.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.guestDnDVersionSet.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools vmxDnDVersionSet disabled"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools vmxDnDVersionSet"
							}
						}
						#VM.disable-vix-messages
						$vix=Get-AdvancedSetting -Name "isolation.tools.vixMessage.disable" -Entity $vm
						if($vix -eq $null){
							New-AdvancedSetting -Name "isolation.tools.vixMessage.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools vixMessage disable"
						}
						else{
							if($vix.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.vixMessage.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools vixMessage disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools vixMessage"
							}
						} Write-Host
					}
				}
				else{
					Write-Warning "[HOST-$HostIP] There is No VirtualMachines"
				}
			}
			if($Global:Action -eq 2){
				#ESXi.verify-acceptance-level-supported
				$ESXCli = Get-EsxCli -VMHost $HostIP -WarningAction silentlycontinue
				$Acceptancelevel=$ESXCli.software.acceptance.get()
				if($Acceptancelevel -ne "VMwareCertified"){
					$ESXCli.software.acceptance.Set("VMwareCertified") | out-null
					Write-Output "[HOST-$HostIP] Set Acceptancelevel-VMwareCertified"
				}
				else{
					Write-Warning "[HOST-$HostIP] Already set Acceptancelevel-VMwareCertified"
				}write-host
			}
			if($Global:Action -eq 3){
				#ESXi.verify-acceptance-level-supported
				$ESXCli = Get-EsxCli -VMHost $HostIP -WarningAction silentlycontinue
				$Acceptancelevel=$ESXCli.software.acceptance.get()
				if($Acceptancelevel -ne "VMwareCertified"){
					$ESXCli.software.acceptance.Set("VMwareCertified") | out-null
					Write-Output "[HOST-$HostIP] Set Acceptancelevel-VMwareCertified"
				}
				else{
					Write-Warning "[HOST-$HostIP] Already set Acceptancelevel-VMwareCertified"
				}write-host
			}
			if(($Global:Action -eq 1) -or ($Global:Action -eq 2)){
				$VMS=Get-VMHost $HostIP | Get-VM 
				Function Get-ParallelPort { 
					Param ( 
						[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] 
						$VM 
					) 
					Process { 
						Foreach ($VMachine in $VM) { 
							Foreach ($Device in $VMachine.ExtensionData.Config.Hardware.Device) { 
								If ($Device.gettype().Name -eq "VirtualParallelPort"){ 
									$Details = New-Object PsObject 
									$Details | Add-Member Noteproperty VM -Value $VMachine 
									$Details | Add-Member Noteproperty Name -Value $Device.DeviceInfo.Label 
									If ($Device.Backing.FileName) { $Details | Add-Member Noteproperty Filename -Value $Device.Backing.FileName } 
									If ($Device.Backing.Datastore) { $Details | Add-Member Noteproperty Datastore -Value $Device.Backing.Datastore } 
									If ($Device.Backing.DeviceName) { $Details | Add-Member Noteproperty DeviceName -Value $Device.Backing.DeviceName } 
									$Details | Add-Member Noteproperty Connected -Value $Device.Connectable.Connected 
									$Details | Add-Member Noteproperty StartConnected -Value $Device.Connectable.StartConnected 
									$Details 
								} 
							} 
						}
					} 
				} 
				Function Remove-ParallelPort { 
					Param ( 
						[Parameter(Mandatory=$True,ValueFromPipelinebyPropertyName=$True)] 
						$VM, 
						[Parameter(Mandatory=$True,ValueFromPipelinebyPropertyName=$True)] 
						$Name 
					) 
					Process { 
						$VMSpec = New-Object VMware.Vim.VirtualMachineConfigSpec 
						$VMSpec.deviceChange = New-Object VMware.Vim.VirtualDeviceConfigSpec 
						$VMSpec.deviceChange[0] = New-Object VMware.Vim.VirtualDeviceConfigSpec 
						$VMSpec.deviceChange[0].operation = "remove" 
						$Device = $VM.ExtensionData.Config.Hardware.Device | Foreach { 
						$_ | Where {$_.gettype().Name -eq "VirtualParallelPort"} | Where { $_.DeviceInfo.Label -eq $Name } 
						} 
						$VMSpec.deviceChange[0].device = $Device 
						$VM.ExtensionData.ReconfigVM_Task($VMSpec) 
					} 
				}
				Function Get-SerialPort { 
					Param ( 
						[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] 
						$VM 
					) 
					Process { 
						Foreach ($VMachine in $VM) { 
							Foreach ($Device in $VMachine.ExtensionData.Config.Hardware.Device) { 
								If ($Device.gettype().Name -eq "VirtualSerialPort"){ 
									$Details = New-Object PsObject 
									$Details | Add-Member Noteproperty VM -Value $VMachine 
									$Details | Add-Member Noteproperty Name -Value $Device.DeviceInfo.Label 
									If ($Device.Backing.FileName) { $Details | Add-Member Noteproperty Filename -Value $Device.Backing.FileName } 
									If ($Device.Backing.Datastore) { $Details | Add-Member Noteproperty Datastore -Value $Device.Backing.Datastore } 
									If ($Device.Backing.DeviceName) { $Details | Add-Member Noteproperty DeviceName -Value $Device.Backing.DeviceName } 
									$Details | Add-Member Noteproperty Connected -Value $Device.Connectable.Connected 
									$Details | Add-Member Noteproperty StartConnected -Value $Device.Connectable.StartConnected 
									$Details 
								} 
							} 
						} 
					} 
				}
				Function Remove-SerialPort { 
					Param ( 
						[Parameter(Mandatory=$True,ValueFromPipelinebyPropertyName=$True)] 
						$VM, 
						[Parameter(Mandatory=$True,ValueFromPipelinebyPropertyName=$True)] 
						$Name 
					) 
					Process { 
						$VMSpec = New-Object VMware.Vim.VirtualMachineConfigSpec 
						$VMSpec.deviceChange = New-Object VMware.Vim.VirtualDeviceConfigSpec 
						$VMSpec.deviceChange[0] = New-Object VMware.Vim.VirtualDeviceConfigSpec 
						$VMSpec.deviceChange[0].operation = "remove" 
						$Device = $VM.ExtensionData.Config.Hardware.Device | Foreach { 
						$_ | Where {$_.gettype().Name -eq "VirtualSerialPort"} | Where { $_.DeviceInfo.Label -eq $Name } 
						} 
						$VMSpec.deviceChange[0].device = $Device 
						$VM.ExtensionData.ReconfigVM_Task($VMSpec) 
					} 
				}
				if($VMS -ne $null){
					foreach($vm in $VMS){
						#VM.disable-independent-nonpersistent
						$NonPersistents=Get-VM $vm | Get-HardDisk | where {$_.Persistence -ne "Persistent"}
						if($NonPersistents -ne $null){
							foreach($NonPersistent in $NonPersistents){
							Get-VM $vm | Get-HardDisk -Name $NonPersistents.Name |  Set-HardDisk -Persistence "Persistent" -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] Disabled-independent-nonpersistent"
							}
						}
						else{
							Write-Warning "[HOST-$HostIP] [VM-$vm] No nonpersistent disk"
						}
						#VM.disable-VMtools-autoinstall
						$AutoInstall=Get-AdvancedSetting -Name "isolation.tools.autoInstall.disable" -Entity $vm
						if($AutoInstall -eq $null){
							New-AdvancedSetting -Name "isolation.tools.autoInstall.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools autoInstall disable"
						}
						else{
							if($AutoInstall.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.autoInstall.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools autoInstall disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools autoInstall"
							}
						}
						$floppys=Get-VM $vm | Get-FloppyDrive
						if($floppys -ne $null){
							foreach($floppy in $floppys){
								Get-VM $vm| Get-FloppyDrive -Name $floppy.Name | Remove-FloppyDrive -Confirm:$false
								Write-Output "[HOST-$HostIP] [VM-$vm] Remove FloppyDrive"
							}
						}
						else{
							Write-Warning "[HOST-$HostIP] [VM-$vm] no floppy drive"
						}
						#VM.disconnect-devices-ParallelPort
						$ParallelPorts=Get-VM $vm| Get-ParallelPort
						if($ParallelPorts -ne $null){
							foreach($ParallelPort in $ParallelPorts){
								Get-VM $vm | Get-ParallelPort -Name $ParallelPort.Name | Remove-ParallelPort -Confirm:$false
								Write-Output "[HOST-$HostIP] [VM-$vm] ParallelPort removed " 
							}
						}
						else{
							Write-Warning "[HOST-$HostIP] [VM-$vm] no ParallelPort"
						}		
						#VM.disconnect-devices-serial
						$SerialPorts=Get-VM $vm| Get-SerialPort
						if($SerialPorts -ne $null){
							foreach($SerialPort in $SerialPorts){
								Get-VM $vm | Get-SerialPort -Name $SerialPort.Name | Remove-SerialPort -Confirm:$false
								Write-Output "[HOST-$HostIP] [VM-$vm] SerialPort removed " 
							}
						}
						else{
							Write-Warning "[HOST-$HostIP] [VM-$vm] no SerialPort"
						}
						#VM.restrict-host-info
						$RestrictHost=Get-AdvancedSetting -Name "tools.guestlib.enableHostInfo" -Entity $vm
						if($RestrictHost -eq $null){
							New-AdvancedSetting -Name "tools.guestlib.enableHostInfo" -value $false -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] Tools guestlib enableHostInfo disable"
						}
						else{
							if($RestrictHost.value -eq $true){
								Get-AdvancedSetting -Name "tools.guestlib.enableHostInfo" -Entity $vm | Set-AdvancedSetting -Value $false -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] Tools guestlib enableHostInfo disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled Tools guestlib enableHostInfo"
							}
						}
						write-host
					}
				}
				else{
					Write-Warning "[HOST-$HostIP] There is No VirtualMachines"
				}
			}		
			if(($Global:Action -eq 1) -or ($Global:Action -eq 2) -or ($Global:Action -eq 3)){
				#Config vCenter NFC useSSL
				#$nfc=Get-AdvancedSetting -Entity $global:vCenter -Name "Config.nfc.useSSL"
				#if($nfc -eq $null){
				#New-AdvancedSetting -Entity $global:vCenter -Name "config.nfc.useSSL" -Value $true -Confirm:$false | out-null
				#	Write-Output "[Server-$global:vCenter] Created New AdvancedSetting for config.nfc.useSSL and set value true"
				#}
				#else{
				#	if($nfc.value -eq $false){
				#		Get-advancedsetting -entity $global:vCenter -name "config.nfc.useSSL" | set-AdvancedSetting -value $true -confirm:$false | out-null
				#		Write-Output "[Server-$vCenterIP] Modified AdvancedSetting config.nfc.useSSL and set value true"
				#	}else{
				#		Write-Warning "[Server-$vCenterIP] Already sdded AdvancedSetting config.nfc.useSSL and set value true"
				#	}
				#}
				# Add NTP Server
				$NTPServer =Get-VMHostNtpServer -VMHost $HostIP
				if($NTPvalue -ne ""){
					foreach($NTP in $NTPvalue.Split(',')){
						if($NTPServer -eq $null){
							$NTPServer =Get-VMHostNtpServer -VMHost $HostIP
							$AddNTP=Add-VMHostNtpServer -VMHost $HostIP -NtpServer $NTP | out-null
							Write-Output "[HOST-$HostIP] Configured NTPserver-$NTP"
						}
						else{
							foreach($getntp in $NTPServer){ 
								if($getntp -eq $NTP){
									Write-Warning "[HOST-$HostIP] Already set NTPserver-$NTP"
								}
							}
						}
					}
					$NTPServer =Get-VMHostNtpServer -VMHost $HostIP
					if($NTPServer -ne $null){
						$CheckNTP=Get-VMHostFirewallException -VMHost $HostIP | where {$_.Name -eq "NTP client"}
						if($CheckNTP.Enabled -ne $true){
						$EnableNTP=Get-VMHostFirewallException -VMHost $HostIP | where {$_.Name -eq "NTP client"} | Set-VMHostFirewallException -Enabled:$true | out-null
						}
						$CheckNTP=Get-VMHostService -VMHost $HostIP | where{$_.Key -match "ntpd"} 
						if($checkNTP.Running -ne $true){
							$startNTP=Get-VMHostService -VMHost $HostIP | where{$_.Key -match "ntpd"} | Start-VMHostService -ErrorAction silentlycontinue | Out-Null
							Write-Output  "[HOST-$HostIP] Started NTP Daemon"
						}
						$CheckPolicy=Get-VMHostService -VMHost $HostIP | where{$_.Key -match "ntpd"}
						if($CheckPolicy.Policy -ne "on"){
							Get-VmHostService -VMHost $HostIP | Where-Object {$_.key -eq "ntpd"} | Set-VMHostService -policy "On" -ErrorAction silentlycontinue | Out-Null
							Write-Output "[HOST-$HostIP] Configuring NTPd"
						}
					}
				}else{
					Write-Warning "[HOST-$HostIP] NTP Value is Null and not Configured"
				}
				#Join Domain
				$DomainStatus=Get-VMHost $HostIP | Get-VMHostAuthentication
				if($DomainStatus.Domain -eq $null){
					if(($Domain -ne "")-and ($DomainAdmin -ne "") -and ($DomainAdminPwd -ne "")){
						Get-VMHostAuthentication -VMHost $HostIP | Set-VMHostAuthentication -Domain $Domain -Username $DomainAdmin -Password $DomainAdminPwd -JoinDomain -Confirm:$false | out-null
						Write-Output "[HOST-$HostIP] Joined Domain-$Domain"
					}else{
						Write-Warning "[HOST-$HostIP] Domain values is Empty and not Configured"
					}
					
				}
				else{
					Write-Warning "[HOST-$HostIP] Already joined Domain-$Domain"
				}
				#Chap Authentication 
				$iscsi=Get-VMHostHba -VMHost $HostIP | Where {$_.Type -eq "Iscsi"}
				if($iscsi -ne $null){
					if(($ChapType -ne "")-and ($ChapName -ne "") -and ($ChapPassword -ne "")){
						Get-VMHostHba -VMHost $HostIP | Where {$_.Type -eq "Iscsi"} | Set-VMHostHba -ChapType $ChapType -ChapName $ChapName -ChapPassword $ChapPassword | out-null
						Write-Output "[HOST-$HostIP] Set Chap Authentication for Iscsi"
					}else{
						Write-Warning "[HOST-$HostIP] Chap values is empty and not configured"
					}
				}
				else{
					Write-Warning "[HOST-$HostIP] No iSCSI Adapter"
				}
				#ESXi.config-persistent-logs
				$Logdir=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Syslog.global.logDir" -WarningAction silentlycontinue 
				if($Logdir.Values -ne $SysLogGlobal)
				{
					if($SysLogGlobal -ne ""){
						Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Syslog.global.logDir" -Value $SysLogGlobal -WarningAction silentlycontinue | out-null
						Write-Output "[HOST-$HostIP] configured-persistent-logs-$SysLogGlobal"
					}else{
						Write-Warning "[HOST-$HostIP] Syslog.global.logDir value is empty and not configured"
					}
					
				}
				else{
					Write-Warning "[HOST-$HostIP] Already set persistent logs-$SysLogGlobal"
				}				
				#Enable SNMP
				$snmp=Get-VMHostService -VMHost $HostIP | where{$_.key -match "snmpd"}
				if($snmp.Running -ne $true){
					$esxcli = Get-EsxCli -VMHost $HostIP
					$esxcli.system.snmp.set($null,"secret","true",$null,$null,$null,$null,$null,$null,$null,$null,$null,$null) | out-null
					Write-Output "[HOST-$HostIP] SNMP Enabled"
					Get-VMHostService -VMHost $HostIP | where{$_.key -match "snmpd"} | Start-VMHostService | out-null
				}else{
					Write-Warning "[HOST-$HostIP] Already started SNMP "
				}
				#Disable MOB
				$MOB=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name Config.HostAgent.plugins.solo.enableMob -WarningAction silentlycontinue 
				if($MOB.Values -eq $true){
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name Config.HostAgent.plugins.solo.enableMob  -Value $false -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] MOB Disabled"
				}
				else{
					Write-Warning "[HOST-$HostIP] Already MOB disabled"
				}		
				#Configure Security AccountUnlockTime
				$AccountTime=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Security.AccountUnlockTime" -WarningAction silentlycontinue
				if($AccountTime.Values -eq 900){
					Write-Warning "[HOST-$HostIP] Already Set Security AccountUnlockTime-900"
				}
				else{
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Security.AccountUnlockTime" -Value 900 -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Configured Security AccountUnlockTime-900"
				}
				#Configure Security AccountUnlockFailures
				$AccountLock=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Security.AccountLockFailures" -WarningAction silentlycontinue
				if($AccountLock.Values -eq 3){
					Write-Warning "[HOST-$HostIP] Already set Security AccountLockFailures-3"			
				}
				else{
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Security.AccountLockFailures" -Value 3 -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Configured Security AccountLockFailures-3"
				}
				#Set DCUI ACCESS local users
				$DCUI=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "DCUI.Access" -WarningAction silentlycontinue
				if($DCUI.Values -ne $LocalUsers){
					if($LocalUsers -ne ""){
						Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "DCUI.Access" -Value $LocalUsers -WarningAction silentlycontinue | out-null
						Write-Output "[HOST-$HostIP] Aet DCUI Access User-$LocalUsers"
					}else{
						Write-Warning "[HOST-$HostIP] LocalUsers value is empty empty and not configured"
					}
				}
				else{
					Write-Warning "[HOST-$HostIP] Already set DCUI Access User-$LocalUsers"
				}
				#Set DcuiTimeOut
				$DCUITimeOut=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.DcuiTimeOut" -WarningAction silentlycontinue
				if($DCUITimeOut.Values -eq 600){
					Write-Warning "[HOST-$HostIP] Already set DCUI TimeOut Value-600"
				}
				else{	
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.DcuiTimeOut" -Value 600 -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Set DCUI TimeOut Value-600"
				}
				#Configure Syslog Server
				$Syslog=Get-VMHostSysLogServer -VMHost $HostIP
				if($Syslog.Host -ne $SysLogServer){
					if($SysLogServer -ne ""){
						Set-VMHostSysLogServer -VMHost $HostIP -SysLogServer $SysLogServer -SysLogServerPort 514  -WarningAction silentlycontinue | out-null
						Write-Output "[HOST-$HostIP] Set Syslogserver $SysLogServer"
					}else{
						Write-Warning "[HOST-$HostIP] Syslogserver value is empty and not configured"
					}
					
				}
				else{
					Write-Warning "[HOST-$HostIP] Already set Syslogserver $SysLogServer"
				}
				#ESXi.set-password-policies
				$Secuity=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Security.PasswordQualityControl" -WarningAction silentlycontinue
				if($Secuity.Values -ne "retry=3 min=8,8,8,7,6"){
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Security.PasswordQualityControl"  -Value "retry=3 min=8,8,8,7,6" -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Set Security PasswordQualityControl value-retry=3 min=8,8,8,7,6"
				}
				else{
					Write-Warning "[HOST-$HostIP] Already set Security PasswordQualityControl value-retry=3 min=8,8,8,7,6"
				}
				#Set ESXiShellInteractiveTimeOut
				$ShelInteractive=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.ESXiShellInteractiveTimeOut" -WarningAction silentlycontinue
				if($ShelInteractive.Values -eq 900){
					Write-Warning "[HOST-$HostIP] Already set ESXiShellInteractiveTimeOut 15 mins"		
				}
				else{
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.ESXiShellInteractiveTimeOut" -Value 900 -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Set ESXiShellInteractiveTimeOut 15 mins "
				}
				# Set Remove UserVars.ESXiShellTimeOut to 900 on all hosts
				$ShelTimeOut=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.ESXiShellTimeOut" -WarningAction silentlycontinue
				if($ShelTimeOut.Values -eq 900){
					Write-Warning "[HOST-$HostIP] Already set ESXiShellTimeOut 15 mins"	
				}
				else{
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "UserVars.ESXiShellTimeOut" -Value 900 -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Set ESXiShellTimeOut 15 mins "
				}
				#ESXi.TransparentPageSharing-intra-enabled
				$Salting=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Mem.ShareForceSalting" -WarningAction silentlycontinue
				if($Salting.Values -eq 2){
					Write-Warning "[HOST-$HostIP] Already set ShareForceSalting value-2"					
				}
				else{
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Mem.ShareForceSalting" -Value 2 -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Set ShareForceSalting value-2"
				}
				#set Net.BlockGuestBPDU 
				$BPDU=Get-VmHostAdvancedConfiguration -VMHost $HostIP -Name "Net.BlockGuestBPDU" -WarningAction silentlycontinue
				if($BPDU.Values -eq 1){
					Write-Warning "[HOST-$HostIP] Already set BlockGuestBPDU value 1"				
				}
				else{
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Net.BlockGuestBPDU" -Value 1 -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Set BlockGuestBPDU value 1"
				}
				#Set DVFilterBindIpAddress value Empty
				$DV=Get-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Net.DVFilterBindIpAddress" -WarningAction silentlycontinue 
				If($DV.Values -ne ""){
					Set-VMHostAdvancedConfiguration -VMHost $HostIP -Name "Net.DVFilterBindIpAddress" -Value "" -WarningAction silentlycontinue | out-null
					Write-Output "[HOST-$HostIP] Set DVFilterBindIpAddress value Empty"
				}
				else{
					Write-Warning "[HOST-$HostIP] Already set DVFilterBindIpAddress value Empty"
				}
				#vNetwork Group Policy 
				$VSS=Get-VirtualSwitch -Standard -VMHost $HostIP 
				foreach($vs in $VSS){
					$GetVS=Get-VirtualSwitch -Standard -Name $vs.Name -VMHost $HostIP | Get-SecurityPolicy
					if(($GetVS.AllowPromiscuous -ne $false) -or ( $GetVS.ForgedTransmits -ne $false) -or ($GetVS.MacChanges -ne $false )){
						Write-Output "[HOST-$HostIP] Set Standard switch Network Policy Forgedtransmit-Reject,Promiscuous-Reject,MacChanges-Reject"
						Get-VirtualSwitch -Standard -Name $vs.Name -VMHost $HostIP | Get-SecurityPolicy | Set-SecurityPolicy -MacChanges $false -ForgedTransmits $false -AllowPromiscuous $false -WarningAction silentlycontinue | out-null
					}
					else{
						Write-Warning "[HOST-$HostIP] Already set standard switch Network Policy Forgedtransmit-Reject,Promiscuous-Reject,MacChanges-Reject"
					}
				}
				#VM.disable-console-copy
				$VMS=Get-VMHost $HostIP | Get-VM 
				if($VMS -ne $null){
					foreach($vm in $VMS){
						$Tools=Get-AdvancedSetting -Name "isolation.tools.copy.disable" -Entity $vm
						if($Tools -eq $null){
							New-AdvancedSetting -Name "isolation.tools.copy.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools copy disable"
						}
						else{
							if($Tools.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.copy.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools copy disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools copy"
							}
						}
						$ToolsDND=Get-AdvancedSetting -Name "isolation.tools.dnd.disable" -Entity $vm
						if($ToolsDND -eq $null){
							New-AdvancedSetting -Name "isolation.tools.dnd.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools dnd disable"
						}
						else{
							if($ToolsDND.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.dnd.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools dnd disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Alreadydisabled isolation tools dnd"
							}
						}
						$ToolsGUI=Get-AdvancedSetting -Name "isolation.tools.setGUIOptions.enable" -Entity $vm
						if($ToolsGUI -eq $null){
							New-AdvancedSetting -Name "isolation.tools.setGUIOptions.enable" -value $false -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools setGUIOptions disable"
						}
						else{
							if($ToolsGUI.value -eq $true){
								Get-AdvancedSetting -Name "isolation.tools.setGUIOptions.enable" -Entity $vm | Set-AdvancedSetting -Value $false -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools setGUIOptions disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools setGUIOptions "
							}
						}
						$ToolsPaste=Get-AdvancedSetting -Name "isolation.tools.paste.disable" -Entity $vm
						if($ToolsPaste -eq $null){
							New-AdvancedSetting -Name "isolation.tools.paste.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools paste disable"
						}
						else{
							if($ToolsPaste.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.paste.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools paste disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools paste"
							}
						}
						$diskShrink=Get-AdvancedSetting -Name "isolation.tools.diskShrink.disable" -Entity $vm
						if($diskShrink -eq $null){
							New-AdvancedSetting -Name "isolation.tools.diskShrink.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools diskShrink disable"
						}
						else{
							if($diskShrink.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.diskShrink.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools diskShrink disable"
							}else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools diskShrink"
							}
						}
						$diskWiper=Get-AdvancedSetting -Name "isolation.tools.diskWiper.disable" -Entity $vm
						if($diskWiper -eq $null){
							New-AdvancedSetting -Name "isolation.tools.diskWiper.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools diskWiper disable" 
						}
						else{
							if($diskWiper.value -eq $false){
								Get-AdvancedSetting -Name "isolation.tools.diskWiper.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation tools diskWiper disable"
							}
							else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation tools diskWiper"
							}
						}
						$sizeLimit=Get-AdvancedSetting -Name "tools.setInfo.sizeLimit" -Entity $vm
						if($sizeLimit -eq $null){
							New-AdvancedSetting -Name "tools.setInfo.sizeLimit" -value 1048576 -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] Set Tools sizeLimit 1048576 "
						}
						else{
							if($sizeLimit.value -eq 1048576){
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already set isolation tools sizeLimit"
							}
							else{
								Get-AdvancedSetting -Name "tools.setInfo.sizeLimit" -Entity $vm | Set-AdvancedSetting -Value 1048576 -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] Set tools sizeLimit 1048576"								
							}
						}		
						$RemoteDisplay=Get-AdvancedSetting -Name "RemoteDisplay.vnc.enabled" -Entity $vm
						if($RemoteDisplay -eq $null){
							New-AdvancedSetting -Name "RemoteDisplay.vnc.enabled" -value $false -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] Set RemoteDisplay vnc minimize console disable"
						}
						else{
							if($RemoteDisplay.value -eq $true){
								Get-AdvancedSetting -Name "RemoteDisplay.vnc.enabled" -Entity $vm | Set-AdvancedSetting -Value $false -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] RemoteDisplay vnc disable"
							}
							else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled RemoteDisplay vnc"
							}
						}						
						$interaction=Get-AdvancedSetting -Name "isolation.device.connectable.disable" -Entity $vm
						if($interaction -eq $null){
							New-AdvancedSetting -Name "isolation.device.connectable.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] VM prevent-device-interaction-connect disable "
						}
						else{
							if($interaction.value -eq $false){
								Get-AdvancedSetting -Name "isolation.device.connectable.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] VM prevent-device-interaction-connect Disabled"
							}
							else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled VM prevent-device-interaction-connect"
							}
						}
						$DeviceEdit=Get-AdvancedSetting -Name "isolation.device.edit.disable" -Entity $vm
						if($DeviceEdit -eq $null){
							New-AdvancedSetting -Name "isolation.device.edit.disable" -value $true -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] isolation device edit disable"
						}
						else{
							if($DeviceEdit.value -eq $false){
								Get-AdvancedSetting -Name "isolation.device.edit.disable" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] isolation device edit disable"
							}
							else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] Already disabled isolation device edit"
							}
						}
						$Pshare=Get-AdvancedSetting -Name "sched.mem.pshare.salt" -Entity $vm
						if($Pshare -eq $null){
							New-AdvancedSetting -Name "sched.mem.pshare.salt" -value 1 -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] TransparentPageSharing-inter Enabled and value 1"
						}
						else{
							if($Pshare.value -eq 1){
								Write-Warning "[HOST-$HostIP] [VM-$vm] TransparentPageSharing-inter Already enabled and value 1"								
							}
							else{
								Get-AdvancedSetting -Name "sched.mem.pshare.salt" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] TransparentPageSharing-inter Enabled and value 1"
							}
						}
						Write-Output "[HOST-$HostIP] [VM-$vm] verify-network-filter "
						Get-AdvancedSetting -Name  "ethernet*.filter*.name*" -Entity $vm | Select Entity, Name, Value
						$pciPassthru=Get-AdvancedSetting -Name "pciPassthru*.present" -Entity $vm
						if($pciPassthru -eq $null){
							New-AdvancedSetting -Name "pciPassthru*.present" -value "" -Entity $vm -Confirm:$false -WarningAction silentlycontinue | out-null
							Write-Output "[HOST-$HostIP] [VM-$vm] verify-PCI-Passthrough"
						}
						else{
							if($pciPassthru.value -ne ""){
								Get-AdvancedSetting -Name "pciPassthru*.present" -Entity $vm | Set-AdvancedSetting -Value $true -Confirm:$false -WarningAction silentlycontinue | out-null
								Write-Output "[HOST-$HostIP] [VM-$vm] verify-PCI-Passthrough and value Empty"
							}
							else{
								Write-Warning "[HOST-$HostIP] [VM-$vm] verify-PCI-Passthrough and value Empty"
							}
						}
						Write-Host
					}
				}
				else{
					Write-Warning "[HOST-$HostIP] There is no VirtualMachines"
				}
				#Function-PGNetflow
				Function Disable-PGNetflow {
					[CmdletBinding()]
					Param (
						[Parameter(ValueFromPipeline=$true)]
						$DVPG
					)
					Process {
						Foreach ($PG in $DVPG) {
							$spec = New-Object VMware.Vim.DVPortgroupConfigSpec
							$spec.configversion = $PG.Extensiondata.Config.ConfigVersion
							$spec.defaultPortConfig = New-Object VMware.Vim.VMwareDVSPortSetting
							$spec.defaultPortConfig.ipfixEnabled = New-Object VMware.Vim.BoolPolicy
							$spec.defaultPortConfig.ipfixEnabled.inherited = $false
							$spec.defaultPortConfig.ipfixEnabled.value = $false
							$PGView = Get-View -Id $PG.Id
							$PGView.ReconfigureDVPortgroup_Task($spec)
						}
					}
				}
				$pgs=Get-VirtualPortGroup -VMHost $HostIP -Distributed -WarningAction silentlycontinue
				foreach($pg in $pgs){
					#vNetwork.restrict-port-level-overrides
					$getover=Get-VDPortgroup $pg.Name | Get-VDPortgroupOverridePolicy
					if(($getover.BlockOverrideAllowed -ne $false) -or ($getover.SecurityOverrideAllowed -ne $false) -or ($getover.VlanOverrideAllowed -ne $false ) -or ($getover.UplinkTeamingOverrideAllowed -ne $false ) -or ($getover.ResetPortConfigAtDisconnect -ne $false))
					{
						Get-VDPortgroup $pg.Name | Get-VDPortgroupOverridePolicy | Set-VDPortgroupOverridePolicy -ResetPortConfigAtDisconnect $false  -BlockOverrideAllowed $false -TrafficShapingOverrideAllowed $false -UplinkTeamingOverrideAllowed $false  -VlanOverrideAllowed $false -SecurityOverrideAllowed $false | out-null
						Write-Output "[VDPortgroup-$pg] Set restrict portlevel overrides-Disabled "
					}
					else{
						Write-Warning "[VDPortgroup-$pg] Already restricted portlevel override policy"
					}
					#Set vdsport group policy
					$GetDvs=Get-VDPortgroup $pg.Name | Get-VDSecurityPolicy 
					if(($GetDvs.AllowPromiscuous -ne $false) -or ( $GetDvs.ForgedTransmits -ne $false) -or ($GetDvs.MacChanges -ne $false )){
						Get-VDPortgroup $pg.Name | Get-VDSecurityPolicy | Set-VDSecurityPolicy -ForgedTransmits $false -AllowPromiscuous $false -MacChanges $false | out-null
						Write-Output "[VDPortgroup-$pg] Set Policy Forgedtransmit-Reject,Promiscuous-Reject,MacChanges-Reject"
					}
					else{
						Write-Warning "[VDPortgroup-$pg] Already set Policy Forgedtransmit-Reject,Promiscuous-Reject,MacChanges-Reject"
					}
					#Disable-PGNetflow
					$Netflow=Get-VDPortgroup $pg.Name
					if($Netflow.ExtensionData.Config.DefaultPortConfig.IpfixEnabled.Value -ne $false){
						Get-VDPortgroup $pg.Name | Disable-PGNetflow  | out-null
						Write-Output "[VDPortgroup-$pg] Disable netflow"
					}
					else{
						Write-Warning "[VDPortgroup-$pg] Already disabled netflow"
					}
				}
			}			
		}
		else{
			Write-Warning "[Host-$HostIP] ERROR: Unable to connect"
		}
	Disconnect-VIServer -Server $HostIP -Confirm:$false
	Write-Host
}
Write-Output "*****************************Completed**************************************"