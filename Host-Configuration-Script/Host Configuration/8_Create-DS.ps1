<#
.SYNOPSIS
Create Datastore for Host

.DESCRIPTION
Create Datastore cluster,Create Datastore using LUNID and set policy method.

.EXAMPLE
 .\8_Create-DS.ps1 -Global Global.csv -InputCSV 8_Create-DS.csv
Global.csv-Input file vCenter details
8_Create-DS.csv-Datastore,Datastore cluster and Host Input

.NOTES
File Name : 7_AddvDS-Host.ps1
Author    : Raj Srinivasan 
Requires  : PowerCLI-6.0
Version	  : 1.0 (19/07/2016)
#>
###################[Inputfile Validation]####################################
	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )	,
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSVFile =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($Global | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$Global doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	if (-not ($InputCSV | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$InputCSV doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}

###################[Initialization]####################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	# Create a Log file in the current dir
	$LogPath = "Storage"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	
###################[Declaring Input File Path]###########################
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$GlobalConfig = Import-Csv -Path $Global
	$InputCSV = Import-Csv -Path $InputCSV 
	
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:VCUser = $null
	$global:VCPassword = $null
	
###########################[Main Function]##############################
	ForEach ($Configvalue in $GlobalConfig) {
		if ($Configvalue.Config -eq "vCenter IP"){
				$global:vCenter =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "vCenter Administrator"){
				$global:VCUser =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "vCenter Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Datacenter Name"){
				$global:Datacenter =$Configvalue.Value.trim()
			}
	}
###########################[Check Input Values]##############################
	if($global:vCenter -eq ""){
		Write-host"vCenter IP Filed is Empty"
		Exit
	}
	if($global:VCUser -eq ""){
		Write-host"vCenter Administrator Filed is Empty"
		Exit
	}
	if($global:VCPassword -eq ""){
		Write-host"vCenter Password Filed is Empty"
		Exit
	}
	if($global:Datacenter -eq ""){
		Write-host"Datacenter Name Filed is Empty"
		Exit
	}

###########################[Connect-VIServer]##############################
	Disconnect-VIServer * -Confirm:$false
	Add-Content -Path $LogPath -Value "STORAGE LOG FILE"
	$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
	If ($results -ne $null){
		Write-Output "[Server $global:vCenter] Connected"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
	}
	else{
		Write-Error "[Server-$global:vCenter] ERROR: Unable to connect"
		Write-Warning "Script execution halted `n"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}
######################[Main Function]################################	
	foreach($Storage in $InputCSV){	
		$DSCluster=$Storage.DSCluster
		$Hosts=$Storage.Hostname
		$DSName=$Storage.DSName 
		$CanonicalName=$Storage.LUNID
		if(($$DSCluster -ne "") -and ($Hosts -ne "") -and ($DSName -ne "") -and ($CanonicalName -ne "")){
			$HostValue=$Hosts.Split(',')
			foreach($HostName in $HostValue){
				$datacenterName=Get-Datacenter -Name $global:Datacenter
				$dataCluster=Get-DatastoreCluster -Name $DSCluster -Location $datacenterName 
				if($dataCluster -eq $null){
					Add-Content -Path $LogPath -Value ""
					Write-Host
					Write-Output "[HOST-$HostName]"
					Add-Content -Path $LogPath -Value "[HOST-$HostName]"
					Write-Output "[Datastorecluster-$DSCluster] Creating"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Datastorecluster-$DSCluster] Creating"
					$DaCluster=New-DatastoreCluster -Name $DSCluster -Location $datacenterName 
				}
				else{
					Write-Host
					Write-Output "[HOST-$HostName]"
					Write-Warning "[Datastorecluster-$DSCluster] Already exists"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Datastorecluster-$DSCluster] Already exists"
				}
				$datastore=Get-Datastore -Name $DSName -Location $datacenterName 
				if($datastore -eq $null){
					$SCSILUN=Get-ScsiLun -VmHost $HostName -CanonicalName $CanonicalName
					if ($SCSILUN -ne $null){
						Write-Output "[HOST-$HostName] Creating Datastore-$DSName"
						Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Creating Datastore-$DSName"
						$Datastore=New-Datastore -Name $DSName -Path $CanonicalName -Vmfs -FileSystemVersion 5 -VMHost $HostName -WarningAction silentlycontinue | Out-Null
						
						Write-Output "[Datastorecluster-$DSCluster] Moving Datastore-$DSName"
						Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Datastorecluster-$DSCluster] Moving Datastore-$DSName"
						$MoveDatastore=Move-Datastore -Datastore $DSName -Destination $DSCluster
					}
					else{
						Write-Warning "[HOST-$HostName] No such name exists in CanonicalName-$CanonicalName"
						Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] No such name exists in CanonicalName-$CanonicalName"
					}
				}
				else{
					Write-Warning "[HOST-$HostName]-[Datastore-$DSName] Already exists"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[Datastore-$DSName] Already exists"
				}
				$policy=Get-VMhost $HostName | Get-ScsiLun -CanonicalName $CanonicalName
				if($policy -ne $null){
					if ($policy.MultipathPolicy -ne "RoundRobin"){
							$setpolicy=Get-VMhost $HostName |Get-ScsiLun -CanonicalName $CanonicalName |Set-ScsiLun -MultipathPolicy RoundRobin -WarningAction silentlycontinue | Out-Null
							Write-Output "[HOST-$HostName]-[Datastore-$DSName]Set MultipathPolicy RoundRobin-$CanonicalName"
							Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[Datastore-$DSName]Set MultipathPolicy RoundRobin-$CanonicalName"
							$policy=Get-VMhost $HostName | Get-ScsiLun -CanonicalName $CanonicalName -WarningAction silentlycontinue | Out-Null
							if ($policy.MultipathPolicy -eq "RoundRobin"){
								$esxcli = Get-EsxCli -VMhost $HostName
								$esxcli.storage.nmp.psp.roundrobin.deviceconfig.set($null,$null,$CanonicalName,1,"iops",$null)
								Write-Output "[HOST-$HostName]-[Datastore-$DSName]Set RoundRobin IOPS value 1-$CanonicalName"
								Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[Datastore-$DSName] Set RoundRobin IOPS value 1-$CanonicalName"
							}
					}
				}
			}
			#Write-Output " Rescan vmfs in $HostName Host"
			#$rescan=Get-VMHostStorage -RescanVmfs -VMHost $HostName
		}
		else{
			Write-Host "Input Value Empty"
		}
		}
	}
	
	