	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$G_CSV =  $(throw "-CSV parameter is required. This is the input csv file with all the Global host configuration."),
		[string]$Host_CSV = $(throw "-CSV parameter is required. This is the input csv file with all the host information.")
	)

	# Check if the input CSV exists!
	if (-not ($G_CSV | Test-Path)) {
		Write-Output "CSV file - $CSV does not exist. Kindly retry with correct input file."
		return
	}
	if (-not ($Host_CSV | Test-Path)) {
		Write-Output "CSV file - $CSV does not exist. Kindly retry with correct input file."
		return
	}

###################[Initialization]####################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	# Create a Log file in the current dir	
	$LogPath = "LicenseLog-"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	# ESXi Variables
	$global:ESXiLicensekey = $null
	$global:ESXiUserName = $null
	$global:ESXiPassword = $null
	$global:Newrootpassword = $null
	$global:ESXiIP = $null
	$global:ESXHostName = $null
	$global:vCenter = $null
	$global:VCUsername = $null
	$global:VCPassword = $null
	
###################[Declaring Input File Path]###########################	
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	
	$GlobalConfig = Import-CSV $G_CSV
	$HostConfig = Import-CSV $Host_CSV
	
###########################[Main Function]##############################
	ForEach ($Configvalue in $GlobalConfig) {
		if($Configvalue.Config -eq "ESX Host Account"){
				$global:ESXiUserName =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "ESX Password"){
				$global:ESXiPassword =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "VC IP"){
				$global:vCenter =$Configvalue.Value
			}	
		Elseif ($Configvalue.Config -eq "New Root Password"){
				$global:Newrootpassword =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "VC Administrator"){
				$global:VCUsername =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "VC Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
	}
	$vCenter =Connect-VIServer -Server $global:vCenter -user $global:VCUsername -Password $global:VCPassword -WarningAction silentlycontinue
	
	If ($vCenter -ne $null){
		Write-Output "[Server-$global:vCenter] Connected"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
		$ServiceInstance = Get-View ServiceInstance
		$LicenseMan= Get-View $ServiceInstance.Content.LicenseManager
		$vSphereLicInfo= @()
		Foreach ($License in $LicenseMan.Licenses){
			$Details="" | Select Name, licenseKey, Total, Used
			$Details.Name=$License.Name
			$Details.licenseKey=$License.licenseKey
			$Details.Total=$License.Total
			If ($Details.Name -eq 'VMware vSphere 6 Enterprise Plus' -And $Details.Total -eq '64') {
				$global:ESXiLicensekey = $License.licenseKey
				Write-Host "[Server-$global:vCenter]-Getting License Key"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Getting License Key"
			}
		}
		#Disconnect-VIServer -Server $global:vCenter -Confirm:$False
		#Write-Output "[Server-$global:vCenter] Disconected"
		#Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Disconected"
	}
	else{
		Write-Error "[Server-$global:vCenter] ERROR: Unable to connect"
		Write-Warning "Script execution halted `n"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}
	if($global:ESXiLicensekey)
	{
		ForEach ($Hostconfigvalue in $HostConfig) {
			$global:ESXiIP = $Hostconfigvalue.IP
			$global:ESXHostName = $Hostconfigvalue.Hostname.trim()
			Add-Content -Path $LogPath -Value $Logvalue "`n***************************** $ESXHostName *********************************"
			Write-Output "`n***************************** $ESXHostName *********************************"
			
			if($global:ESXiIP){
			Write-Output "[HOST-$global:ESXiIP] Connected"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:ESXiIP] Connected"
					Write-Output "[HOST-$global:ESXiIP] Configuring  License"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Host-$global:ESXHostName] Configuring  License"
					Set-VMHost -VMHost $global:ESXiIP -LicenseKey $global:ESXiLicensekey -WarningAction silentlycontinue -ErrorAction SilentlyContinue | Out-Null
					Write-Output "[HOST-$global:ESXiIP] Configured vCenter license "
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-)]-[HOST-$global:ESXiIP] Configured vCenter license"
				
				}
		}
	}
	else{
		Write-Output "[Server-$global:vCenter]License not available to perform the operation."
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] ERROR: License not available to perform the operation."
	}