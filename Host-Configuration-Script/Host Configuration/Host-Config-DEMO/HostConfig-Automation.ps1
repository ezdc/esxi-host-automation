<#
.SYNOPSIS
Configuring Host in vCenter

.DESCRIPTION
Adding host into the cluster of vCenter,Configuring Host settings like DNS,NTP,Change root password,create service account and enable syslog server.
Adds the host into distributedswitch and migrates pNIC and vNIC and delete the VSS.
Create Datastores for all host,enable roundrobin poilcy and set IOPS value to be 1.

.EXAMPLE
.\HostConfig-Automation.ps1 .\HostConfig-Automation.CSV .\Datastore.CSV
HostConfig-Automation.CSV-Input file for all the Host details
Datastore.CSV-Datastore creation input file all the Host

.NOTES
File Name : HostConfig-Automation.ps1
Author    : Raj Srinivasan 
Requires  : PowerCLI 
Version	  : 6.0
#>

[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSVFile =  $(throw "-CSV parameter is required. This is the input csv file." ),
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$StorageCSVFile =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($InputCSVFile | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$InputCSVFile doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	if (-not ($StorageCSVFile | Test-Path)) {
    # file with path $path doesn't exist
	Write-Output "$StorageCSVFile doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}

###################[Initialization]####################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	# Create a Log file in the current dir
	$LogPath = "HostConfig-Automation"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
###################[Declaring Input File Path]###########################
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$global:HostConfig=$null
	$global:vCenter=$null
	$global:VCPassword=$null
	$global:VCUsername=$null
	$global:Cluster=$null
	$global:Datacenter=$null
	$global:Hostname=$null
	$global:HostIP=$null
	$global:HostAccount=$null
	$global:HostPassword=$null
	$global:NewPassword=$null
	$global:VLAN=$null
	$global:Gateway=$null
	$global:Subnet=$null
	$global:DNS=$null
	$global:MAC=$null
	$global:IPv6Mode=$null
	$global:DNSSuffixes=$null
	$global:NTPService=$null
	$global:AuthenticationServices=$null
	$global:ServiceAccount=$null
	$global:ServicePassword=$null
	$global:SyslogServer=$null
	$global:SwitchName=$null
	$global:Portgroup=$null
	$global:Datastore=$null
	$global:LUNID=$null
	$global:DSCluster=$null
	$global:MultipathPolicy=$null
	$global:Licensekey=$null
	$global:Datastores=$null
	
########################[SubRoutine]#####################################

########################[Get-Input-Details]##############################
Function Get-Input-Details{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Reading csv to collect required data..."
  }

  Process{
	Try{
		#Collect information from CSV file
		$global:vCenter=$global:HostConfig.VCIP
		$global:VCUsername=$global:HostConfig.VCAdministrator
		$global:VCPassword=$global:HostConfig.VCPassword
		$global:Cluster=$global:HostConfig.Cluster
		$global:Datacenter=$global:HostConfig.Datacenter
		$global:Hostname=$global:HostConfig.Hostname
		$global:HostIP=$global:HostConfig.HostIP
		$global:HostAccount=$global:HostConfig.HostAccount
		$global:HostPassword=$global:HostConfig.HostPassword
		$global:NewPassword=$global:HostConfig.NewPassword
		$global:VLAN=$global:HostConfig.VLAN
		$global:Gateway=$global:HostConfig.Gateway
		$global:Subnet=$global:HostConfig.Subnet
		$global:DNS=$global:HostConfig.DNS
		$global:MAC=$global:HostConfig.MAC
		$global:IPv6Mode=$global:HostConfig.IPv6Mode
		$global:DNSSuffixes=$global:HostConfig.DNSSuffixes
		$global:NTPService=$global:HostConfig.NTPService
		$global:AuthenticationServices=$global:HostConfig.AuthenticationServices
		$global:ServiceAccount=$global:HostConfig.ServiceAccount
		$global:ServicePassword=$global:HostConfig.ServicePassword
		$global:SyslogServer=$global:HostConfig.SyslogServer
		$global:SwitchName=$global:HostConfig.SwitchName
		$global:Portgroup=$global:HostConfig.Portgroup
		$global:Datastore=$global:HostConfig.Datastore
		$global:LUNID=$global:HostConfig.LUNID
		$global:DSCluster=$global:HostConfig.DSCluster
		$global:MultipathPolicy=$global:HostConfig.MultipathPolicy
		Add-Content -Path $LogPath -Value "`n*****************************  $global:HostIP  *********************************"
		Write-Output "`n*****************************  $global:HostIP  *********************************"
	}
	
	Catch{
	  Break
	}
  }
}

########################[Connect-vCenter]################################
Function Connect-vCenter{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Connecting vCenter"
  }

  Process{
	Try{
		#Connect vCenter Server
		$results = Connect-VIServer -Server $global:vCenter -user $global:VCUsername -Password $global:VCPassword -WarningAction silentlycontinue
		If($results -eq $null){
			Write-Host "ERROR: Unable to connect to the vCenter Server-$global:vCenter"
			Write-Host "Script execution halted `n"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] ERROR: Unable to connect to the vCenter Server-$global:vCenter"
			exit
		}
		else{
			Write-Host
			Write-Output "[Server-$global:vCenter] Connected"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
		}
	}
	Catch{
 
	  Break
	}
  }
}

########################[Host-Configuration]#############################
Function Host-Config{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Start Host Configuration..."
  }

  Process{
	Try{
	   # Add Host to datacenter and cluster ,login with root credentials
	   Write-Host
		$VMHost = Get-VMHost -Name $global:HostIP -Location (Get-Datacenter $global:Datacenter) -ErrorAction SilentlyContinue
		if($VMHost -eq $null){
			$Addhost=Add-VMHost -Name $global:HostIP -Location (Get-Datacenter $global:Datacenter | Get-Cluster $global:Cluster) -User $global:HostAccount -Password $global:HostPassword -Force -Confirm:$false -ErrorAction SilentlyContinue | Out-Null	
			Write-Output "[Server-$global:vCenter]-[HOST:$global:HostIP] Adding"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]-[HOST:$global:HostIP] Adding"				
		}
		else{
			Write-Warning "[Host-$global:HostIP] Already Exists"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Host:$global:HostIP] Already Exists"
		}
		#Disconnect vCenter Server
		Write-Output "[Server-$global:vCenter] Disconected "
		Disconnect-VIServer -Server $global:vCenter -Confirm:$False
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server $global:vCenter] Disconected"
		
		#Connect to ESXi server
		$Con = Connect-VIServer $global:HostIP -username $global:HostAccount -Password $global:HostPassword -ErrorAction SilentlyContinue -WarningAction silentlycontinue	
		If ($Con -ne $null){
			Write-Host
			Write-Output "[HOST-$global:HostIP] Connected"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Connected"
			$vmHost = Get-VMHost -Name $global:HostIP
			
			$DSName = Get-Datastore -Name *-local-* | select Name -ErrorAction silentlycontinue
			Remove-Datastore -Datastore $DSName.Name -VMHost $global:HostIP -Confirm:$false -ErrorAction silentlycontinue -WarningAction silentlycontinue | Out-Null
			Write-Output "[HOST-$global:HostIP] Removed local datastore"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Removed local datastore"
			
			# Set DNS Server
			Write-Output  "[HOST-$global:HostIP] Configuring DNS Server"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Host-$global:HostIP] Configuring DNS Server"
			$DNS=$global:DNS.Split(',')
			$DNS1=$DNS[0]
			$DNS2=$DNS[1]
			$DNSVal= Get-VMHostNetwork | Set-VMHostNetwork -HostName $global:Hostname -DomainName $global:DNSSuffixes -DNSAddress $DNS1,$DNS2 -Confirm:$false -ErrorAction silentlycontinue -WarningAction silentlycontinue #| Out-Null
			Write-Output "[HOST-$global:HostIP] Configured DNSserver-Prefer:$DNS1,Alternate:$DNS1"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Configured DNSserver-Prefer:$DNS1,Alternate:$DNS1"
			
			# Add NTP Server
			Write-Output "[HOST-$global:HostIP] Configuring NTP server"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Configuring NTP server"
			$NTPServer =Get-VMHostNtpServer
			if($NTPServer -eq $null){
				$AddNTP=Add-VMHostNtpServer -NtpServer $DNS1,$DNS2
				Write-Output "[HOST-$global:HostIP] Configured NTPserver-$DNS1,$DNS2"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Configured NTPserver-$DNS1,$DNS2"
			}
			else{
				if($NTPServer[0] -ne $DNS1 -And $NTPServer[1] -ne $DNS2){
					Add-VMHostNtpServer -NtpServer $DNS1,$DNS2
					Write-Output "[HOST:$global:HostIP] Configured NTP server-$DNS1,$DNS2"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST:$global:HostIP] Configured NTP server-$DNS"
				}
				else{
					Write-Warning "[HOST:$global:HostIP] NTPserver-$DNS Already exists"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] The NtpServer-$DNS already exists"
				}
			}
			$NTPServer =Get-VMHostNtpServer
			if($NTPServer -ne $null){
				$enableNTP=Get-VMHostFirewallException | where {$_.Name -eq "NTP client"} | Set-VMHostFirewallException -Enabled:$true | out-null
				$startNTP=Get-VMHostService | where{$_.Key -match "ntpd"} | Start-VMHostService -ErrorAction silentlycontinue | Out-Null
				Write-Output  "[HOST-$global:HostIP] Started NTP Daemon"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Started NTP Daemon"
				if($global:NTPService -eq "Start and stop with host"){
					$setpolicy=Get-VmHostService | Where-Object {$_.key -eq "ntpd"} | Set-VMHostService -policy "On" -ErrorAction silentlycontinue | Out-Null
					Write-Output  "[HOST-$global:HostIP] Changed NTP Policy-Start and stop with host"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Changed NTP Policy-Start and stop with host"
				}
			}
			
			#Set Timeout for Tech Support Mode
			Write-Output "[HOST-$global:HostIP] Setting Timeout for Tech Support Mode"
			Get-VMHost -Name $global:HostIP | Set-VMHostAdvancedConfiguration -Name "UserVars.ESXiShellTimeOut" 900 -ErrorAction silentlycontinue -WarningAction silentlycontinue | Out-Null
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Setting Timeout for Tech Support Mode 900 milliseconds"
				
			#Set SYSLOG Settings
			Write-Output "[HOST-$global:HostIP] Setting SYSLOG settings for SysLogServer,default size and rotate"
			Get-VMHost -Name $global:HostIP | Set-VMHostAdvancedConfiguration -Name "Syslog.global.defaultSize" 5120 -ErrorAction silentlycontinue -WarningAction silentlycontinue | Out-Null
			Get-VMHost -Name $global:HostIP| Set-VMHostAdvancedConfiguration -Name "Syslog.global.defaultRotate" 20 -ErrorAction silentlycontinue -WarningAction silentlycontinue | Out-Null
			Get-VMHost -Name $global:HostIP | Set-VMHostSysLogServer -SysLogServer $global:SyslogServer -SysLogServerPort 514 -ErrorAction silentlycontinue -WarningAction silentlycontinue | Out-Null
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Setting SYSLOG settings for SysLogServer,default size and rotate"
				
			#Add service Account
			$user = Get-VIAccount -User -Id $global:ServiceAccount -ErrorAction silentlycontinue
			if($user -eq $null){
				try{
					$addAcc=New-VMHostAccount -Id $global:ServiceAccount -Password $global:ServicePassword -Description "Test Account" -ErrorAction silentlycontinue | Out-Null
					$setPermission=Get-VMHost -Name $global:HostIP | New-VIPermission -Principal $global:ServiceAccount -Role Admin -ErrorAction silentlycontinue | Out-Null
					Write-Output "[HOST-$global:HostIP] Added Service Account with admin permission"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Added Service Account with admin permission"
				}
				catch{
					Write-Warning "[HOST:$global:HostIP] Service Account Not added-User name or password has an invalid format"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST:$global:HostIP] Service Account Not added-User name or password has an invalid format"
				}
			}
			else{
				Write-Warning  "[HOST:$global:HostIP]]-[ServiceAccount-global:ServiceAccount] Already exists"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP]]-[ServiceAccount-global:ServiceAccount] Already exists"
			}
				#Change root Password
			Write-Host "[HOST-$global:HostIP] Changing root password"
			Set-VMHostAccount -UserAccount root -password $global:NewPassword -Confirm:$false -ErrorAction silentlycontinue | Out-Null
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Changing root password"	
				#Set to Maintenance Mode
			Write-Host "[HOST-$global:HostIP] Enter Maintenance Mode"
			Set-VMHost -State Maintenance -ErrorAction silentlycontinue | Out-Null
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Enter Maintenance Mode"
				#Disconnect from ESXi server
			Disconnect-VIServer $global:HostIP -confirm:$false
			Write-Host "[HOST-$global:HostIP] Disconnected"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Disconnected"
		}
		else{
				Write-Host "[HOST-$global:HostIP] ERROR: Unable to connect,incorrect username and password"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] ERROR: Unable to connect,incorrect username and password"
			}	
	}
	
	Catch{
      Break
	}
  }
}

########################[Update-Host-Licence]############################
Function Update-Licence{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Updating Host License..."
  }

  Process{
	Try{
		$ServiceInstance = Get-View ServiceInstance
		$LicenseMan= Get-View $ServiceInstance.Content.LicenseManager
		$vSphereLicInfo= @()
		Foreach ($License in $LicenseMan.Licenses){
			$Details="" | Select Name, licenseKey, Total, Used
			$Details.Name=$License.Name
			$Details.licenseKey=$License.licenseKey
			$Details.Total=$License.Total
			If ($Details.Name -eq 'VMware vSphere 6 Enterprise Plus' -And $Details.Total -eq '64') {
				$global:Licensekey = $License.licenseKey
				Write-Host
				Write-Output "[Server-$global:vCenter] Getting License Key-$global:Licensekey"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Getting License Key-$global:Licensekey"
			}
		}
		Write-Output "[HOST-$global:HostIP] Configuring  License"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Host-$global:HostIP] Configuring  License"
		Set-VMHost -VMHost $global:HostIP -LicenseKey $global:Licensekey -WarningAction silentlycontinue -ErrorAction SilentlyContinue | Out-Null
		Write-Output "[HOST-$global:HostIP] Configured vCenter license "
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Configured vCenter license"
	}
	Catch{
	   Break
	}
  }
}

########################[Update-vDS-Switch]##############################
Function Update-vDS-Switch{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Updating vDS Switch..."
  }
  
  Process{
	Try{
		$Datacenter=Get-Datacenter -Name $global:Datacenter
		$dvsSwitch =Get-VDSwitch -Name $global:SwitchName -Location $Datacenter
		$PG = Get-VDPortgroup -name $global:Portgroup -VDSwitch $dvsSwitch		
		$vmnic=Get-VMHost -Name $global:HostIP -Location $Datacenter | Get-VMHostNetworkAdapter -Physical
		$vmk=Get-VMHost -Name $global:HostIP -Location $Datacenter | Get-VirtualSwitch  -Name "vSwitch0" | Get-VMHostNetworkAdapter  -VMKernel
		$AddHost=Add-VDSwitchVMHost -VMHost $global:HostIP  -VDSwitch $dvsSwitch
		Write-Host
		Write-Output "[HOST-$global:HostIP] Adding to $global:SwitchName vDSSwitch"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Adding to $global:SwitchName vDSSwitch"
		if($vmnic[1] -ne $null){
				#Get Physical Nic Adapter from Host
			$vmhostadapter = Get-VMHost $global:HostIP -Location $Datacenter| Get-VMHostNetworkAdapter -Physical -Name $vmnic[1]
				#Add PhysicalNic to DVS
			$MigrationNICs=Add-VDSwitchPhysicalNetworkAdapter -DistributedSwitch $dvsSwitch -VMHostPhysicalNic $vmhostadapter -Confirm:$false
			Write-Output "[HOST-$global:HostIP] Adding vmnic1 nic in $global:SwitchName vDSSwitch"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Adding vmnic1 nic in $global:SwitchName vDSSwitch"
				#Move VMKernelNic to portGroup
			$setvmk=Set-VMHostNetworkAdapter -PortGroup $PG -VirtualNic $vmk -confirm:$false 
			Write-Output "[HOST-$global:HostIP] Migrating VMKernelNic $vmk in $global:Portgroup PortGroup"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Migrating VMKernelNic $vmk in $global:Portgroup PortGroup"
		}
		if($vmnic[0] -ne $null){
			$RemoveNIC=Get-VMHost $global:HostIP -Location $Datacenter | Get-VMHostNetworkAdapter -Physical -Name $vmnic[0]  | Remove-VirtualSwitchPhysicalNetworkAdapter -Confirm:$false
			Write-Output "[HOST-$global:HostIP] Removed vmnic0 Physical nic in standard switch"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Removed vmnic0 nic in standard switch"
			$vmhostadapter = Get-VMHost $global:HostIP -Location $Datacenter | Get-VMHostNetworkAdapter -Physical -Name $vmnic[0]
			$MigrationNICs=Add-VDSwitchPhysicalNetworkAdapter -DistributedSwitch $dvsSwitch -VMHostPhysicalNic $vmhostadapter -Confirm:$false
			Write-Output "[HOST-$global:HostIP] Added $vmnic0 nic in $global:SwitchName vDSSwitch"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Added vmnic0 nic in $global:SwitchName vDSSwitch"
			$RemoveSwitch=Get-VMHost -Name $global:HostIP -Location $Datacenter| Get-VirtualSwitch -Name "vSwitch0" | Remove-VirtualSwitch -Confirm:$false
			Write-Output "[HOST-$global:HostIP] Deleted vSwitch0 "
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Deleted vSwitch0"
		}
			$vhost = Get-VMHost $global:HostIP -Location $Datacenter
			$uplinks = $vhost | Get-VDSwitch $dvsSwitch | Get-VDPort -Uplink | where {$_.ProxyHost -like $vhost.Name}
			$config = New-Object VMware.Vim.HostNetworkConfig
			$config.proxySwitch = New-Object VMware.Vim.HostProxySwitchConfig[] (1)
			$config.proxySwitch[0] = New-Object VMware.Vim.HostProxySwitchConfig
			$config.proxySwitch[0].changeOperation = "edit"
			$config.proxySwitch[0].uuid = $dvsSwitch.Key
			$config.proxySwitch[0].spec = New-Object VMware.Vim.HostProxySwitchSpec
			$config.proxySwitch[0].spec.backing = New-Object VMware.Vim.DistributedVirtualSwitchHostMemberPnicBacking
			$config.proxySwitch[0].spec.backing.pnicSpec = New-Object VMware.Vim.DistributedVirtualSwitchHostMemberPnicSpec[] (2)
			$config.proxySwitch[0].spec.backing.pnicSpec[0] = New-Object VMware.Vim.DistributedVirtualSwitchHostMemberPnicSpec
			$config.proxySwitch[0].spec.backing.pnicSpec[0].pnicDevice = $vmnic[0]
			$config.proxySwitch[0].spec.backing.pnicSpec[0].uplinkPortKey = ($uplinks | where {$_.Name -eq "dvUplink1"}).key
			$config.proxySwitch[0].spec.backing.pnicSpec[1] = New-Object VMware.Vim.DistributedVirtualSwitchHostMemberPnicSpec
			$config.proxySwitch[0].spec.backing.pnicSpec[1].pnicDevice = $vmnic[1]
			$config.proxySwitch[0].spec.backing.pnicSpec[1].uplinkPortKey = ($uplinks | where {$_.Name -eq "dvUplink2"}).key
			$_this = Get-View (Get-View $vhost).ConfigManager.NetworkSystem
			$update=$_this.UpdateNetworkConfig($config, "modify")
	}
	
	Catch{
	  Break
	}
  }
}

########################[Create-Datastore]################################
Function Create-Datastore{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Create Datastore..."
  }

  Process{
	Try{
		$LUNS= $global:Datastores | Where-Object { $_.HostIP -eq $global:HostIP}
		foreach ($Lun in $LUNS){
			$DSName=$Lun.DSName
			$CanonicalName=$Lun.LUNID
			$DSCluster=$Lun.DSCluster
			$HostName=$Lun.HostIP
			$datacenterName=Get-Datacenter -Name $global:Datacenter
			$dataCluster=Get-DatastoreCluster -Name $DSCluster -Location $datacenterName -ErrorAction silentlycontinue
			Write-Host
		   if($dataCluster -eq $null){
				Write-Output "[Datastorecluster-$DSCluster] Creating"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Datastorecluster-$DSCluster] Creating"
				$DaCluster=New-DatastoreCluster -Name $DSCluster -Location $datacenterName 
			}
			#else{
			#	Write-Host
			#	Write-Warning "[Datastorecluster-$DSCluster] Already exists"
			#	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Datastorecluster-$DSCluster] Already exists"
			#}
			$datastore=Get-Datastore -Name $DSName -Location $datacenterName -ErrorAction silentlycontinue
			
			if($datastore -eq $null){
				$SCSILUN=Get-ScsiLun -VmHost $HostName -CanonicalName $CanonicalName -LunType disk
				if ($SCSILUN -ne $null){
					Write-Output "[HOST-$HostName] Creating Datastore-$DSName"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Creating Datastore-$DSName"
					$Datastore=New-Datastore -Name $DSName -Path $CanonicalName -Vmfs -FileSystemVersion 5 -VMHost $HostName
					
					Write-Output "[Datastorecluster-$DSCluster] Moving Datastore-$DSName"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Datastorecluster-$DSCluster] Moving Datastore-$DSName"
					$MoveDatastore=Move-Datastore -Datastore $DSName -Destination $DSCluster
				}
				else{
					Write-Warning "[HOST-$HostName] No such name exists in CanonicalName-$CanonicalName"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] No such name exists in CanonicalName-$CanonicalName"
				}
			}
			else{
				Write-Warning "[HOST-$HostName]-[Datastore-$DSName] Already exists"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[Datastore-$DSName] Already exists"
			}
			$policy=Get-VMhost $HostName | Get-ScsiLun -CanonicalName $CanonicalName -LunType disk
				if ($policy.MultipathPolicy -ne "RoundRobin"){
						$setpolicy=Get-VMhost $HostName |Get-ScsiLun -CanonicalName $CanonicalName |Set-ScsiLun -MultipathPolicy RoundRobin
						Write-Output "[HOST-$HostName]-[Datastore-$DSName]Set MultipathPolicy RoundRobin-$CanonicalName"
						Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[Datastore-$DSName]Set MultipathPolicy RoundRobin-$CanonicalName"
						$policy=Get-VMhost $HostName | Get-ScsiLun -CanonicalName $CanonicalName
				}
			$policy=Get-VMhost $HostName | Get-ScsiLun -CanonicalName $CanonicalName -LunType disk
			if($policy.MultipathPolicy -eq "RoundRobin"){
				$esxcli = Get-EsxCli -VMhost $HostName
				$esxcli.storage.nmp.psp.roundrobin.deviceconfig.set($null,$null,$CanonicalName,1,"iops",$null)
				Write-Output "[HOST-$HostName]-[Datastore-$DSName]Set RoundRobin IOPS value 1-$CanonicalName"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[Datastore-$DSName] Set RoundRobin IOPS value 1-$CanonicalName"
			}
			#Get-VMHost -Name $global:HostIP | Set-VMHost -State Connected -ErrorAction silentlycontinue | Out-Null
			#Write-Output "[HOST-$global:HostIP] Exit Maintenance Mode `n"
			#Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostIP] Exit Maintenance Mode `n"
			
		}
		Disconnect-VIServer * -Confirm:$false -ErrorAction silentlycontinue	
	}
	
	Catch{
	  Break
	}
  }
}

########################[Execution]#######################################
	$InputCSV = Import-Csv -Path $InputCSVFile
	$global:Datastores=Import-Csv -Path $StorageCSVFile
	Disconnect-VIServer * -Confirm:$false -ErrorAction silentlycontinue
	
	foreach($global:HostConfig in $InputCSV){	
		Get-Input-Details
		
		Connect-vCenter
		
		Host-Config
		
		Connect-vCenter
		
		Update-Licence
	
		Update-vDS-Switch
		
		Create-Datastore
		
	}