<#
.SYNOPSIS
Create Datastore for Host

.DESCRIPTION
Create Datastore cluster,Create Datastore using LUNID and set policy method.

.EXAMPLE
 .\ResolveHost.ps1 -Global Global.csv -InputCSV 2_Host-Config.csv
Global.csv-Input file vCenter details
2_Host-Config.csv-ESXI host and domain deatils

.NOTES
File Name : ResolveHost.ps1
Author    : Raj Srinivasan 
Requires  : PowerCLI-6.0
Version	  : 6.0
TODO: Log file (Change Log file Checking hostname in domain,host IP is resloved)
#>
###################[Inputfile Validation]####################################
	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )	,
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSV =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($Global | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$Global doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	if (-not ($InputCSV| Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$InputCSV doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	
###################[Declaring Input File Path]###########################

	$GlobalConfig = Import-Csv -Path $Global
	$Input = Import-Csv -Path $InputCSV
	
	$global:DNS=$null
###########################[Main Function]##############################
	ForEach ($Configvalue in $GlobalConfig) {
		if ($Configvalue.Config -eq "Preferred DNS"){
				$global:DNS =$Configvalue.Value.trim()
		}
	}
	
######################[Main Function]################################	
	foreach($DNS in $Input)
	{	
		$dot="."
		$Hostname=$DNS.Hostname
		$Domain=$DNS.Domain 
		$NewHost=$Hostname+ $dot + $Domain
		$DNSvalue=Resolve-DnsName -Name $NewHost -Server $global:DNS -DnsOnly
		if($DNSvalue.Name -ne $null){
			Write-Output "[Host-$Hostname] Resolved host name in DNS"
		}else{
			Write-Warning "[Host-$Hostname] not Resolve Host name"
		}

	}
	
	