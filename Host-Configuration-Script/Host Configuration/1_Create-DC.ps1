<#
.SYNOPSIS
	Create datacenter and Cluster

.DESCRIPTION
	Create datacenter and Cluster for the vCenter

.EXAMPLE
	.\1_Create-DC.ps1 -Global Global.csv 
	Global.csv-Input file vCenter details

.NOTES
	File Name : 1_Create-DC.ps1
	Author    : Raj Srinivasan 
	Requires  : PowerCLI-6.0
	Version	  : 1.0 (19/07/2016)
#>

[CmdletBinding(SupportsShouldProcess=$true)]
param (
	[Parameter(Mandatory=$True)]
	[ValidateNotNull()]
	[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )
)
if (-not ($Global | Test-Path)) {
	# file with path $path doesn't exist
	Write-Output "$Global doesnot exist. Kindly retry with correct file"
	exit
}
	clear
	# Create a Log file in the current dir
	$LogPath = "CreateDC-Cluster-"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}

	$Error.Clear()
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green 
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:VCUser = $null
	$global:VCPassword = $null
	$global:Clusters=$null
	$global:Folder=$null
	$value=$null

###########################[Main Function]##############################
$GlobalConfig = Import-Csv -Path $Global
ForEach ($Configvalue in $GlobalConfig) {
		if ($Configvalue.Config -eq "vCenter IP"){
				$global:vCenter =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "vCenter Administrator"){
				$global:VCUser =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "vCenter Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Datacenter Name"){
				$global:Datacenter =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Cluster Name"){
				$global:Clusters =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Folder name"){
				$global:Folder =$Configvalue.Value.trim()
			}		
	}
###########################[Check Input Values]##############################
	if($global:vCenter -eq ""){
		Write-host"vCenter IP Filed is Empty"
		Exit
	}
	if($global:VCUser -eq ""){
		Write-host"vCenter Administrator Filed is Empty"
		Exit
	}
	if($global:VCPassword -eq ""){
		Write-host"vCenter Password Filed is Empty"
		Exit
	}
	if($global:Datacenter -eq ""){
		Write-host"Datacenter Name Filed is Empty"
		Exit
	}
	if($global:Clusters -eq ""){
		Write-host"Cluster Name Filed is Empty"
		Exit
	}
	if($global:Folder -eq ""){
		Write-host"Folder name Filed is Empty"
		Exit
	}

###########################[Connect vCenter]##############################
	Disconnect-VIServer * -Confirm:$false
	
	Add-Content -Path $LogPath -Value "CREATE DATACENTER AND CLUSTER LOG FILE"
	$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
	If ($results -ne $null){
		Write-Output "[vCenter-$global:vCenter] Connected"
		Write-Host
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vCenter-$global:vCenter] Connected"
	}
	else{
		Write-Error "[vCenter-$global:vCenter] ERROR: Unable to connect"
		Write-Warning "Script execution halted `n"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vCenter-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}
###########################[Main Function]##############################

	#Create Datacenter
	if($value -eq $null){
		$ValidDC = Get-Datacenter -Name $global:Datacenter -ErrorAction SilentlyContinue
		if($ValidDC -eq $null){
			$CreateDC = New-Datacenter -Location (Get-Folder -NoRecursion) -Name $global:Datacenter
			Write-Output "[Datacenter-$Datacenter] Created"
			Write-Host
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Datacenter-$Datacenter] Created"
		}
		else{
			Write-Output "[Datacenter-$Datacenter] Already exists"
			Write-Host
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Datacenter-$Datacenter] Already exists"
		}
		$value=0
	}
	if($value -eq 0){
		#Create Cluster
		$Clusters = $global:Clusters.Split(',')
		
		Foreach ($Cluster in $Clusters)
		{
			$ValidCluster = Get-Cluster -Name $Cluster -Location (Get-Datacenter -Name $global:Datacenter) -ErrorAction SilentlyContinue
			if($ValidCluster -eq $null){
				$CreateCluster = New-Cluster -Name $Cluster -Location (Get-Datacenter -Name $global:Datacenter)   
				Write-Output "[Cluster-$Cluster] Created"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Cluster-$Cluster] Created"	
			}
			else{ 
				Write-Output "[Cluster-$Cluster] Already exists"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Cluster-$Cluster] Already exists"
			}	
		}
		$value=1
	}
	if($value -eq 1){
		#Create Floder
		$Folders= $global:Folder.Split(',')
		Foreach ($Folder in $Folders){	
			$ValidFolder = Get-Folder -Name $Folder -Location (Get-Datacenter -Name $global:Datacenter) -ErrorAction SilentlyContinue
			if($ValidFolder -eq $null){
				New-Folder -Name $Folder -Location (Get-Datacenter -Name $global:Datacenter) | Out-Null
				Write-Host
				Write-Output "[Folder-$Folder] Created"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Folder-$Folder] Created"
			}
			else{
				Write-Host
				Write-Output "[Folder-$Folder] Already exists"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Folder: $Folder] Already exists"
			}
		}
		Write-Host
		Write-Output "[vCenter-$global:vCenter] Disconected "
		Disconnect-VIServer -Server $global:vCenter -Confirm:$False
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vCenter-$global:vCenter] Disconected"
	}
	
	