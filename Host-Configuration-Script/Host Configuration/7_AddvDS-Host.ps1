<#
.SYNOPSIS
Add Host into distributed switch

.DESCRIPTION
Add Host into distributed switch,Migrates vmkernel Nic and physical Nic to dVS and delete the vSS

.EXAMPLE
 .\7_AddvDS-Host.ps1 -Global Global.csv -InputCSV 6_Create-PG.csv
Global.csv-Input file vCenter details
6_Create-PG.csv-Switch,Portgroup and  Host Input file

.NOTES
File Name : 7_AddvDS-Host.ps1
Author    : Raj Srinivasan 
Requires  : PowerCLI-6.0
Version	  : 1.0(19/07/2016)
#>
###################[Inputfile Validation]####################################
	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )	,
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSV =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($Global | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$Global doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	if (-not ($InputCSV | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$InputCSV doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}

#####################[Initialization]#################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	# Create a Log file in the current dir
	$LogPath = "vDS Switch"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	
###################[Declaring Input]#################################
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$GlobalConfig = Import-Csv -Path $Global
	$InputCSV = Import-Csv -Path $InputCSV
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:VCUser = $null
	$global:VCPassword = $null
	$global:VSS=$null
	
###########################[Connect vCenter]#############################

	ForEach ($Configvalue in $GlobalConfig) {
		if ($Configvalue.Config -eq "vCenter IP"){
				$global:vCenter =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "vCenter Administrator"){
				$global:VCUser =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "vCenter Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Datacenter Name"){
				$global:Datacenter =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Standard Switch"){
				$global:VSS =$Configvalue.Value.trim()
			}
	}
###########################[Check Input Values]##############################
	if($global:vCenter -eq ""){
		Write-host"vCenter IP Filed Empty"
		Exit
	}
	if($global:VCUser -eq ""){
		Write-host"vCenter Administrator Filed Empty"
		Exit
	}
	if($global:VCPassword -eq ""){
		Write-host"vCenter Password Filed Empty"
		Exit
	}
	if($global:Datacenter -eq ""){
		Write-host"Datacenter Name Filed Empty"
		Exit
	}
	if($global:Config -eq ""){
		Write-host"Standard Switch Filed Empty"
		Exit
	}

###########################[Connect-VIServer]##############################	
	Disconnect-VIServer * -Confirm:$false
	Add-Content -Path $LogPath -Value "Network-ADD Host in vDS SWITCH LOG FILE"
	$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
	If ($results -ne $null){
		Write-Output "[Server $global:vCenter] Connected"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
	}
	else{
		Write-Error "[Server-$global:vCenter] ERROR: Unable to connect"
		Write-Warning "Script execution halted `n"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}
###########################[Main Function]############################
	foreach($Network in $InputCSV){	
		$dvs=$Network.SwitchName
		$dvportgroup=$Network.Portgroup
		$Clusters=$Network.ClusterName
		$PgType=$Network.PGType
		$nic=$Network.NIC
		if(($Clusters -ne "") -and ($dvs -ne "") -and ($dvportgroup -ne "")){
			$ClusterValue=$Clusters.Split(',')
			foreach($Cluster in $ClusterValue){
				$HostValue=Get-Cluster -Name $Cluster | Get-VMHost		
				foreach($HostName in $HostValue){
					$Datacenter=Get-Datacenter -Name $global:Datacenter
					$dvsSwitch =Get-VDSwitch -Name $dvs -Location $Datacenter
					$PG = Get-VDPortgroup -name $dvportgroup -VDSwitch $dvsSwitch
					Add-Content -Path $LogPath -Value ""	
					Write-Host
					Write-Output "[HOST-$HostName]"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]"
					if(($nic -ne "") -and ($PgType -ne"")){
						$pNICs=$nic.Split(',')
						$vmnic=$null
						foreach($pNIC in $pNICs){
							$vmhostadapter = Get-VMHost $HostName -Location $Datacenter| Get-VMHostNetworkAdapter -Physical -Name $pNIC
							$switchvalue=Get-VDSwitch -Name $dvsSwitch | Get-VMHost -Name $HostName
							if($switchvalue -eq $null){
								$AddHost=Add-VDSwitchVMHost -VMHost $HostName -VDSwitch $dvsSwitch
								Write-Output "[HOST-$HostName]-[vDSSwitch-$dvs] Adding"
								Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[vDSSwitch-$dvs] Adding"
							}
							else{
								Write-Output "[HOST-$HostName]-[vDSSwitch-$dvs] Already added"
								Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[vDSSwitch-$dvs] Already added"
							}
							#Get Physical Nic Adapter from Host
							$vmk=Get-VMHost -Name $HostName -Location $Datacenter | Get-VirtualSwitch  -Name "vSwitch0" | Get-VMHostNetworkAdapter  -VMKernel
							if($PgType -eq "Management"){
								if($pNIC -ne "vmnic0"){
										#Add PhysicalNic to DVS
									$MigrationNICs=Add-VDSwitchPhysicalNetworkAdapter -DistributedSwitch $dvsSwitch -VMHostPhysicalNic $vmhostadapter -Confirm:$false
									Write-Output "[HOST-$HostName]-[vDSSwitch-$dvs] Adding PhysicalNic-$vmhostadapter"
									Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[vDSSwitch-$dvs] Adding PhysicalNic-$vmhostadapter"
										#Move VMKernelNic to portGroup
									$setvmk=Set-VMHostNetworkAdapter -PortGroup $PG -VirtualNic $vmk[0] -confirm:$false 
									Write-Output "[HOST-$HostName]-[PortGroup-$dvportgroup] Migrating VMKernelNic-vmk0"
									Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[PortGroup-$dvportgroup] Migrating VMKernelNic-vmk0"
									$vmnic=$pNIC
								}
								if($pNIC -eq "vmnic0"){
									$RemoveNIC=Get-VMHost $HostName -Location $Datacenter | Get-VMHostNetworkAdapter -Physical -Name $pNIC  | Remove-VirtualSwitchPhysicalNetworkAdapter -Confirm:$false
									Write-Output "[HOST-$HostName]-[vSS-vSwitch0] Remove PhysicalNic-vmnic0"
									Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[vSS-vSwitch0] Remove PhysicalNic-vmnic0"
									
									$MigrationNICs=Add-VDSwitchPhysicalNetworkAdapter -DistributedSwitch $dvsSwitch -VMHostPhysicalNic $vmhostadapter -Confirm:$false
									Write-Output "[HOST-$HostName]-[vDSSwitch-$dvs] Adding PhysicalNic-vmnic0"
									Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[vDSSwitch-$dvs] Adding PhysicalNic-vmnic0"
									
									if($global:VSS -eq "No"){
										$RemoveSwitch=Get-VMHost -Name $HostName -Location $Datacenter| Get-VirtualSwitch -Name "vSwitch0" | Remove-VirtualSwitch -Confirm:$false
										Write-Output "[HOST-$HostName]-[vSS-vSwitch0] Deleted"
										Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[vSS-vSwitch0] Deleted"
									}
									$vhost = Get-VMHost $HostName -Location $Datacenter
									$uplinks = $vhost | Get-VDSwitch $dvsSwitch | Get-VDPort -Uplink | where {$_.ProxyHost -like $vhost.Name}
									$config = New-Object VMware.Vim.HostNetworkConfig
									$config.proxySwitch = New-Object VMware.Vim.HostProxySwitchConfig[] (1)
									$config.proxySwitch[0] = New-Object VMware.Vim.HostProxySwitchConfig
									$config.proxySwitch[0].changeOperation = "edit"
									$config.proxySwitch[0].uuid = $dvsSwitch.Key
									$config.proxySwitch[0].spec = New-Object VMware.Vim.HostProxySwitchSpec
									$config.proxySwitch[0].spec.backing = New-Object VMware.Vim.DistributedVirtualSwitchHostMemberPnicBacking
									$config.proxySwitch[0].spec.backing.pnicSpec = New-Object VMware.Vim.DistributedVirtualSwitchHostMemberPnicSpec[] (2)
									$config.proxySwitch[0].spec.backing.pnicSpec[0] = New-Object VMware.Vim.DistributedVirtualSwitchHostMemberPnicSpec
									$config.proxySwitch[0].spec.backing.pnicSpec[0].pnicDevice = "vmnic0"
									$config.proxySwitch[0].spec.backing.pnicSpec[0].uplinkPortKey = ($uplinks | where {$_.Name -eq "dvUplink1"}).key
									$config.proxySwitch[0].spec.backing.pnicSpec[1] = New-Object VMware.Vim.DistributedVirtualSwitchHostMemberPnicSpec
									$config.proxySwitch[0].spec.backing.pnicSpec[1].pnicDevice = $vmnic
									$config.proxySwitch[0].spec.backing.pnicSpec[1].uplinkPortKey = ($uplinks | where {$_.Name -eq "dvUplink2"}).key
									$_this = Get-View (Get-View $vhost).ConfigManager.NetworkSystem
									$update=$_this.UpdateNetworkConfig($config, "modify")
									$vmnic=$null
								}		
							}
							if($PgType -eq "vMotion"){
								$MigrationNICs=Add-VDSwitchPhysicalNetworkAdapter -DistributedSwitch $dvsSwitch -VMHostPhysicalNic $vmhostadapter -Confirm:$false
								Write-Output "[HOST-$HostName]-[vDSSwitch-$dvs] Adding PhysicalNic-$vmhostadapter"
								Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[vDSSwitch-$dvs] Adding PhysicalNic-$vmhostadapter"
								
								#Move VMKernelNic to portGroup
								$setvmk=Set-VMHostNetworkAdapter -PortGroup $PG -VirtualNic $vmk[1] -confirm:$false 
								Write-Output "[HOST-$HostName]-[PortGroup-$dvportgroup] Migrating VMKernelNic-vmk0"
								Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[PortGroup-$dvportgroup] Migrating VMKernelNic-vmk0"
							}
							if($PgType -eq "VMTraffic"){
								#$MigrationNICs=Add-VDSwitchPhysicalNetworkAdapter -DistributedSwitch $dvsSwitch -VMHostPhysicalNic $vmhostadapter -Confirm:$false
								#Write-Output "[HOST-$HostName]-[vDSSwitch-$dvs] Adding PhysicalNic-$vmhostadapter"
								#Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]-[vDSSwitch-$dvs] Adding PhysicalNic-$vmhostadapter"
							}
						}
					}
				}
			}
			# Get-VDSwitch -Name $dvs | Remove-VDSwitchVMHost -VMHost $ExsiHost -Confirm:$false
		}
		else{
			Write-Host "Input Value Empty"
		}
	}