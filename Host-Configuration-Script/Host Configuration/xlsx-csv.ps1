$excelFile =  Read-Host "Specify the excel File path?"
$csvLoc =Read-Host "Specify the CSV File path?"
$E = New-Object -ComObject Excel.Application
$E.Visible = $false
$E.DisplayAlerts = $false

# Check if temp directory exists else create this based on timestamp
$wb = $E.Workbooks.Open($excelFile)
foreach ($ws in $wb.Worksheets) {
    $ws.SaveAs($csvLoc + $ws.Name + ".csv", 6)
}
$E.Quit()
