<#
.SYNOPSIS
Create portgroup

.DESCRIPTION
Create portgroup for all distributed switch

.EXAMPLE
 .\6_Create-PG.ps1 -Global Global.csv -InputCSV 6_Create-PG.csv
Global.csv-Input file vCenter details
6_Create-PG.csv-portgroup Input file

.NOTES
File Name : 6_Create-PG.ps1
Author    : Raj Srinivasan 
Requires  : PowerCLI-6.0
Version	  : 1.0(19/07/2016)
#>
###################[Inputfile Validation]####################################
 	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )	,
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSV =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($Global | Test-Path)) {
    # file with path $path doesn't exist
	Write-Output "$Global doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	if (-not ($InputCSV | Test-Path)) {
    # file with path $path doesn't exist
	Write-Output "$InputCSV doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}

###################[Initialization]####################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	
	# Create a Log file in the current dir
	$LogPath = "PortGroup"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
###################[Declaring Input]###########################
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$GlobalConfig = Import-Csv -Path $Global
	$InputCSV = Import-Csv -Path $InputCSV 
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:VCUser = $null
	$global:VCPassword = $null
	
###########################[Connect vCenter]##############################
	ForEach ($Configvalue in $GlobalConfig) {
		if ($Configvalue.Config -eq "vCenter IP"){
				$global:vCenter =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "vCenter Administrator"){
				$global:VCUser =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "vCenter Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Datacenter Name"){
				$global:Datacenter =$Configvalue.Value.trim()
			}
		
	}
###########################[Check Input Values]##############################
	if($global:vCenter -eq ""){
		Write-host"vCenter IP Filed is Empty"
		Exit
	}
	if($global:VCUser -eq ""){
		Write-host"vCenter Administrator Filed is Empty"
		Exit
	}
	if($global:VCPassword -eq ""){
		Write-host"vCenter Password Filed is Empty"
		Exit
	}
	if($global:Datacenter -eq ""){
		Write-host"Datacenter Name Filed is Empty"
		Exit
	}

###########################[Connect-VIServer]##############################	
	Disconnect-VIServer * -Confirm:$false
	Add-Content -Path $LogPath -Value "NETWORK LOG FILE"
	$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
	If ($results -ne $null){
		Write-Output "[Server $global:vCenter] Connected"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
	}
	else{
		Write-Error "[Server-$global:vCenter] ERROR: Unable to connect"
		Write-Warning "Script execution halted `n"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}
	
######################[Main Function]###############################
	foreach($Network in $InputCSV)
	{
		$Switch=$Network.SwitchName
		$Portgroup=$Network.Portgroup
		$vlanID=$Network.VLAN
		if(($Switch -ne "") -and ($Portgroup -ne "") -and ($vlanID -ne "")){
			$datacenterName=Get-Datacenter -Name $global:Datacenter		
			Add-Content -Path $LogPath -Value ""	
			Write-Host
			Write-Output "[vDSwitch-$Switch]"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch]"
			$vDS=Get-VDSwitch -Name $Switch -Location $datacenterName
			Write-Output "[vDSwitch-$Switch] Getting switch details"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch] Getting switch details"
			if($vDS -ne $null){
				$PG=New-VDPortgroup -Name $Portgroup -VlanId $vlanID -VDSwitch $vDS
				if($PG -ne $null){
					Write-Output "[vDSwitch-$Switch]-[PortGroup-$Portgroup] Creating"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch]-[PortGroup-$Portgroup] Creating"
					#$SecurityPolicy=Get-VDPortgroup -Name $Portgroup -VDSwitch $vDS | Get-VDSecurityPolicy | Set-VDSecurityPolicy -AllowPromiscuous $true -ForgedTransmits $false -MacChanges $false
					#$vmk=New-VMHostNetworkAdapter -VMHost $VMHost -PortGroup $Portgroup -VirtualSwitch $vDS -IP $IP -SubnetMask $subnet
					}
				else{
					Write-Warning "[vDSwitch-$Switch]-[PortGroup-$Portgroup] Already exists"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch]-[PortGroup-$Portgroup]  Already exists"
				}
			}
			else{
				Write-Warning "[vDSwitch-$Switch] No such name exists"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch] No such name exists"
			}
		}
		else{
			Write-Host "Input Value Empty"
		}
	}
	