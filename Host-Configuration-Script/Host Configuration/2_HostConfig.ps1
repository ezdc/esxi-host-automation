<#
.SYNOPSIS
	Configuring Host in vCenter

.DESCRIPTION
	Adding host into the cluster of vCenter,Configuring Host settings like DNS,NTP,Change root password,
	create service account and enable syslog server.

.EXAMPLE
	.\2_HostConfig.ps1 -Global Global.CSV -InputCSV HostConfig.CSV 
	HostConfig.CSV- Input file for all the Host details
	Global.CSV- vCenter details and common details

.NOTES
	File Name : 2_HostConfig.ps1
	Author    : Raj Srinivasan 
	Requires  : PowerCLI-6.0 
	Version	  : 1.0 (19/07/2016)
#>

[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )	,
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSV =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($Global | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$Global doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	if (-not ($InputCSV | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$InputCSV doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	clear
	# Create a Log file in the current dir
	$LogPath = "Host-Configuration"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	$Error.Clear()
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green 
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$global:ESXiUserName = $null
	$global:ESXiPassword = $null
	$global:ServiceAccount = $null
	$global:ServicePassword = $null
	$global:DNS = $null
	$global:DNSSuffixes = $null
	$global:NTP = $null
	$global:SyslogServer = $null
	$global:DomainName = $null
	$global:DomainUsername = $null
	$global:DomainPasssword = $null
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:Newrootpassword = $null
	$global:PreferredDNS = $null
	$global:AlternateDNS =$null
	$global:VCPassword =$null
	$global:VCUser = $null
	$global:Licensekey=$null
	$global:Cluster =$null
	$global:HostName =$null
	$global:HostName=$null
	$global:DatastoreOption=$null
	
########################[Get-Input-Details]##############################
Function Get-Input-Details{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Reading csv to collect required data..."
  }

  Process{
	Try{
		#Collect information from CSV file
		ForEach($Configvalue in $global:Config){
		if($Configvalue.Config -eq "ESX Host Account"){
				$global:ESXiUserName =$Configvalue.Value
			}
		Elseif($Configvalue.Config -eq "ESX Password"){
				$global:ESXiPassword =$Configvalue.Value
			}
		Elseif($Configvalue.Config.trim() -eq "Service Account"){
				$global:ServiceAccount =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config.trim() -eq "Service Account Password"){
				$global:ServicePassword =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config -eq "Preferred DNS"){
				$global:PreferredDNS =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config -eq "Alternate DNS"){
				$global:AlternateDNS =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config -eq "Custom DNS Suffixes"){
				$global:DNSSuffixes =$Configvalue.Value
				$global:DomainName =$Configvalue.Value
			}
		Elseif($Configvalue.Config.trim() -eq "NTP Servers"){
				$global:NTP =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config -eq "Syslog Server"){
				$global:SyslogServer =$Configvalue.Value
			}
		Elseif($Configvalue.Config -eq "Domain UserName"){
				$global:DomainUsername =$Configvalue.Value
			}
		Elseif($Configvalue.Config -eq "Domain Passsword"){
				$global:DomainPasssword =$Configvalue.Value
			}
		Elseif($Configvalue.Config -eq "vCenter IP"){
				$global:vCenter =$Configvalue.Value
			}
		Elseif($Configvalue.Config -eq "Datacenter Name"){
				$global:Datacenter =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config -eq "New Root Password"){
				$global:Newrootpassword =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config -eq "vCenter Administrator"){
				$global:VCUser =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config -eq "vCenter Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
		Elseif($Configvalue.Config -eq "Local Datatore"){
				$global:DatastoreOption =$Configvalue.Value.trim()
			}
	}

	}
	
	Catch{
	  Break
	}
  }
}

########################[Connect-vCenter]################################
Function Connect-vCenter{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Connecting vCenter"
  }

  Process{
		#Connect vCenter Server
		$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
		If ($results -ne $null){
			Write-Output "[vCenter-$global:vCenter] Connected"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vCenter-$global:vCenter] Connected"
		}
		else{
			Write-Error "[vCenter-$global:vCenter] ERROR: Unable to connect"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vCenter-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
			exit
		}
	}
}

########################[Connect-vCenter]################################
Function Disconnect-vCenter{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Connecting vCenter"
  }

  Process{
		#Disconnect vCenter Server
		Write-Output "[vCenter-$global:vCenter] Disconected "
		Disconnect-VIServer -Server $global:vCenter -Confirm:$False
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vCenter-$global:vCenter] Disconected"
	}
}

########################[Add Host]########################################	
Function AddHost-vCenter{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Adding Host to vCenter"
  }

  Process{
		#Connect vCenter Server
		if($global:HostName -ne ""){
			$dot="."
			$HostName=$global:HostName + $dot + $global:DomainName
			$VMHost = Get-VMHost -Name $HostName -Location (Get-Datacenter $global:Datacenter) -ErrorAction SilentlyContinue
			if($VMHost -eq $null){
				
				Add-VMHost -Name $HostName -Location (Get-Datacenter $global:Datacenter | Get-Cluster -Name $global:Cluster) -User $global:ESXiUserName -Password $global:ESXiPassword -Force -Confirm:$false | Out-Null
				Write-Output "[vCenter-$global:vCenter]-[HOST-$global:HostName] Added"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vCenter-$global:vCenter]-[HOST-$global:HostName ] Added"					
			}
			else{
				Write-Warning "[Host-$global:HostName] Already exists"
				$Logvalue = "[$([DateTime]::Now)]-[Host-$global:HostName] Already exists"
				Add-Content -Path $LogPath -Value $Logvalue
			}
		}
		else{
			Write-Warning "HostName Input Value empty"
		}
	}

}	

########################[Host-Configuration]##############################
Function Host-Config{
  Param()

  Begin{
	Add-Content -Path $LogPath -Value ""
	Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] Start Host Configuration..."
  }

  Process{
	Try{
	   # Add Host to datacenter and cluster ,login with root credentials
		Write-Host	
		$HostName = $global:HostName
		Write-Output "`n*****************************[$HostName]*********************************"
		Add-Content -Path $LogPath -Value "`n*****************************[$HostName]*********************************"
		# Connect to ESXi server
		if($HostName -ne ""){
			$Con = Connect-VIServer $HostName -username $global:ESXiUserName -Password $global:ESXiPassword -ErrorAction SilentlyContinue -WarningAction silentlycontinue
			If ($Con -ne $null){
				Write-Host
				Write-Output "[HOST-$HostName] Connected"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Connected"	
				$vmHost = Get-VMHost -Name $HostName
				if($global:DatastoreOption -eq "No"){
					$DSName = Get-Datastore -Name *-local-* | select Name -ErrorAction silentlycontinue
					Remove-Datastore -Datastore $DSName.Name -VMHost $HostName -Confirm:$false -ErrorAction silentlycontinue -WarningAction silentlycontinue | Out-Null
					Write-Output "[HOST-$HostName] Removed local datastore"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Removed local datastore"
				}
				# Set DNS Server
				Write-Output  "[HOST-$HostName] Configuring DNS Server"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Host-$HostName] Configuring DNS Server"
				$ADNS=$global:AlternateDNS.Split(',')
				if(($global:DomainName -ne "") -and ($global:PreferredDNS -ne "") -and ($ADNS[0] -ne "")){
					$DNSVal = Get-VMHostNetwork | Set-VMHostNetwork -HostName $HostName -DomainName $global:DomainName -DNSAddress $global:PreferredDNS,$ADNS[0] -Confirm:$false -ErrorAction silentlycontinue -WarningAction silentlycontinue
					Write-Output "[HOST-$HostName] Configured DNSserver-Prefer:$global:PreferredDNS,Alternate:$ADNS[0]"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Configured DNSserver-Prefer:$global:PreferredDNS,Alternate:$ADNS[0]"
				}
				# Add NTP Server
				Write-Output "[HOST-$HostName] Configuring NTP server"
				$Logvalue = "[$([DateTime]::Now)]-[HOST-$HostName] Configuring NTP server"
				Add-Content -Path $LogPath -Value $Logvalue
				$NTPServer = Get-VMHostNtpServer -ErrorAction silentlycontinue -WarningAction SilentlyContinue
				if($global:NTP -ne ""){
					foreach($NTP in $global:NTP.Split(',')) { 
						if($NTPServer -eq $null){
							try{
								Add-VMHostNtpServer -NtpServer $NTP.trim() -Confirm:$false -ErrorAction silentlycontinue -WarningAction SilentlyContinue | Out-Null
								Write-Output "[HOST-$HostName] Configured NTPserver-$NTP"
								$Logvalue = "[$([DateTime]::Now)]-[HOST-$HostName] Configured NTPserver-$NTP"
								Add-Content -Path $LogPath -Value $Logvalue
							}
							catch{
								Write-Warning "[HOST-$HostName] The NtpServer '$NTP' already exists"
								$Logvalue = "[$([DateTime]::Now)]-[HOST-$HostName] The NtpServer '$NTP' already exists"
								Add-Content -Path $LogPath -Value $Logvalue
							}
						}
						else {	
							foreach($getntp in $NTPServer){ 
								if($getntp -eq $NTP.trim()){
									Write-Warning "[HOST-$HostName] NTPserver-$NTP Already exists"
									$Logvalue = "[$([DateTime]::Now)]-[HOST-$HostName] NTPserver-$NTP Already exists"
									Add-Content -Path $LogPath -Value $Logvalue
								}
								else{
									try{
										Add-VMHostNtpServer -NtpServer $NTP.trim() -Confirm:$false -ErrorAction silentlycontinue -WarningAction SilentlyContinue | Out-Null
										Write-Output "[HOST-$HostName] Configured NTP server-$NTP"
										$Logvalue = "[$([DateTime]::Now)]-[HOST-$HostName] Configured NTP server-$NTP"
										Add-Content -Path $LogPath -Value $Logvalue
									}
									catch{
										Write-Warning "[HOST-$HostName]-[NtpServer-$NTP] Already exists"
										$Logvalue = "[$([DateTime]::Now)]-[HOST-$HostName]-[NtpServer-$NTP] Already exists"
										Add-Content -Path $LogPath -Value $Logvalue
									}
								}
							}
						}
					}
					$NTPServer =Get-VMHostNtpServer
					if($NTPServer -ne $null){
						$enableNTP=Get-VMHostFirewallException | where {$_.Name -eq "NTP client"} | Set-VMHostFirewallException -Enabled:$true | out-null
						$startNTP=Get-VMHostService | where{$_.Key -match "ntpd"} | Start-VMHostService -ErrorAction silentlycontinue | Out-Null
						Write-Output  "[HOST-$HostName] Started NTP Daemon"
						Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Started NTP Daemon"
						if($global:NTPService -eq "Start and stop with host"){
							$setpolicy=Get-VmHostService | Where-Object {$_.key -eq "ntpd"} | Set-VMHostService -policy "On" -ErrorAction silentlycontinue | Out-Null
							Write-Output  "[HOST-$HostName] Changed NTP Policy-Start and stop with host"
							Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Changed NTP Policy-Start and stop with host"
						}
					}
				}
				#Set Timeout for Tech Support Mode
				Write-Output "[HOST-$HostName] Setting Timeout for Tech Support Mode"
				
				Get-AdvancedSetting -Name "UserVars.ESXiShellTimeOut" -Entity $HostName | Set-AdvancedSetting -Value 900 | Out-Null
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Setting Timeout for Tech Support Mode 900 milliseconds"
					
				#Set SYSLOG Settings
				Write-Output "[HOST-$global:HostName] Setting SYSLOG settings for SysLogServer,default size and rotate"
				Get-AdvancedSetting -Name "Syslog.global.defaultSize" -Entity $HostName | Set-AdvancedSetting -Value 5120 | Out-Null
				Get-AdvancedSetting -Name "Syslog.global.defaultRotate" -Entity $HostName | Set-AdvancedSetting -Value 20 | Out-Null
				Get-VMHost -Name $HostName | Set-VMHostSysLogServer -SysLogServer $global:SyslogServer -SysLogServerPort 514 -ErrorAction silentlycontinue -WarningAction silentlycontinue | Out-Null
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$global:HostName] Setting SYSLOG settings for SysLogServer,default size and rotate"
					
				#Add service Account
				$user = Get-VIAccount -User -Id $global:ServiceAccount -ErrorAction silentlycontinue
				if($user -eq $null){
					try{
						if(($global:ServiceAccount -ne "") -and ($global:ServicePassword -ne "")){
							$addAcc=New-VMHostAccount -Id $global:ServiceAccount -Password $global:ServicePassword -Description "Test Account" -ErrorAction silentlycontinue | Out-Null
							$setPermission=Get-VMHost -Name $HostName | New-VIPermission -Principal $global:ServiceAccount -Role Admin -ErrorAction silentlycontinue | Out-Null
							Write-Output "[HOST-$HostName] Added Service Account with admin permission"
							Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Added Service Account with admin permission"
						}
					}
					catch{
						Write-Warning "[HOST:$HostName] Service Account Not added-User name or password has an invalid format"
						Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST:$HostName] Service Account Not added-User name or password has an invalid format"
					}
				}
				else{
					Write-Warning  "[HOST-$HostName]]-[ServiceAccount-global:ServiceAccount] Already exists"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName]]-[ServiceAccount-global:ServiceAccount] Already exists"
				}
					#Change root Password
				if($global:Newrootpassword -ne ""){
					Write-Host "[HOST-$HostName] Changing root password"
					Set-VMHostAccount -UserAccount root -password $global:Newrootpassword -Confirm:$false -ErrorAction silentlycontinue | Out-Null
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Changing root password"	
				}
				#Set to Maintenance Mode
				Write-Host "[HOST-$HostName] Enter Maintenance Mode"
				Set-VMHost -State Maintenance -ErrorAction silentlycontinue | Out-Null
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Enter Maintenance Mode"
					#Disconnect from ESXi server
				Disconnect-VIServer $HostName -confirm:$false
				Write-Host "[HOST-$HostName] Disconnected"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$HostName] Disconnected"
			}		
		}
		else{
			Write-Warning "HostName Input Value empty"
		}
	}
	Catch{
      Break
	}
  }
}

########################[Execution]#######################################
	$global:Config = Import-CSV $Global
	$HostConfigs = Import-CSV $InputCSV	
	Disconnect-VIServer * -Confirm:$false -ErrorAction silentlycontinue
	Get-Input-Details
	Connect-vCenter
	foreach($HostConfig in $HostConfigs){	
		$global:Cluster = $HostConfig.Cluster
		$global:HostName = $HostConfig.Hostname
		$global:Domain=$HostConfig.Domain
		AddHost-vCenter		
	}
	
	Disconnect-vCenter
	
	foreach($HostConfig in $HostConfigs){	
		$global:HostIP = $HostConfig.IP
		$global:HostName = $HostConfig.Hostname
		
		Host-Config	
	}