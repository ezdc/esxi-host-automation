<#
.SYNOPSIS
	Create Distributed switch

.DESCRIPTION
	Create Distributed switch

.EXAMPLE
.\5_Create-vDS.ps1 -Global Global.CSV -InputCSV 5_Create-vDS.CSV
	5_Create-vDS.CSV- Input file for vDS switch 
	Global.CSV- vCenter Details and common details

.NOTES
	File Name : 5_Create-vDS
	Author    : Raj Srinivasan 
	Requires  : PowerCLI-6.0
	Version	  : 1.0(19/07/2016)
#>
########################[Initial Validation]################################
	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )	,
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSV =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($Global | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$Global doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}
	if (-not ($InputCSV | Test-Path)) {
    # file with path $path doesn't exist
	Write-Host "$InputCSV doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}

###################[Initialization]####################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	# Create a Log file in the current dir
	$LogPath = "Create vDS Switch"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	
###################[Declaring Input File Path]###########################
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$GlobalConfig = Import-Csv -Path $Global
	$InputCSV = Import-Csv -Path $InputCSV 
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:VCUser = $null
	$global:VCPassword = $null
	
###########################[Connect vCenter]########################

	ForEach ($Configvalue in $GlobalConfig) {
		if ($Configvalue.Config -eq "vCenter IP"){
				$global:vCenter =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "vCenter Administrator"){
				$global:VCUser =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "vCenter Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Datacenter Name"){
				$global:Datacenter =$Configvalue.Value.trim()
			}
	}
###########################[Check Input Values]##############################
	if($global:vCenter -eq ""){
		Write-host"vCenter IP Filed is Empty"
		Exit
	}
	if($global:VCUser -eq ""){
		Write-host"vCenter Administrator Filed is Empty"
		Exit
	}
	if($global:VCPassword -eq ""){
		Write-host"vCenter Password Filed is Empty"
		Exit
	}
	if($global:Datacenter -eq ""){
		Write-host"Datacenter Name Filed is Empty"
		Exit
	}

###########################[Connect-VIServer]##############################	
	Disconnect-VIServer * -Confirm:$false
	Add-Content -Path $LogPath -Value "Create vDS SWITCH LOG FILE"
	$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
	If ($results -ne $null){
		Write-Output "[Server $global:vCenter] Connected"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
	}
	else{
		Write-Error "[Server-$global:vCenter] ERROR: Unable to connect"
		Write-Warning "Script execution halted `n"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}
	
###########################[Main Function]########################
	foreach($Network in $InputCSV){	
		$Switch=$Network.SwitchName
		$Uplinks=$Network.Uplinks
		if(($Switch -ne "") -and ($Uplinks -ne ""){
			$datacenterName=Get-Datacenter -Name $global:Datacenter
			Add-Content -Path $LogPath -Value ""	
			Write-Host
			Write-Output "[vDSwitch-$Switch]"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch]"
			$Datacenter=Get-Datacenter -Name $global:Datacenter
			$dvsSwitch =Get-VDSwitch -Name $Switch -Location $Datacenter
			if($dvsSwitch -eq $null){
				Write-Output "[vDSwitch-$Switch] Creating"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch] Creating"
				$vDS=New-VDSwitch -Name $Switch -Location $datacenterName -NumUplinkPorts $Uplinks
				Write-Output "[vDSwitch-$Switch] dVSswitch created"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch] dVSswitch created"
			}
			else{
				Write-Warning "[vDSwitch-$Switch]  Already Exists"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch] Already Exists"
			}
		}
		else{
			Write-Host "Input Value Empty"
		}
	}