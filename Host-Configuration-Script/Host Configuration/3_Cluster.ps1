<#
.SYNOPSIS
Configuring Cluster

.DESCRIPTION
Configure Cluster settings like HA,DRS,SwapFilePolicy and EVC

.EXAMPLE
.\3_Cluster.ps1 -Global Global.CSV -InputCSV Cluster.CSV 
3_Cluster.CSV- Input file for all the Cluster details
Global.CSV- vCenter Details and common details

.NOTES
File Name : 3_Cluster.ps1
Author    : Raj Srinivasan 
Requires  : PowerCLI-6.0
Version	  : 1.0 (19/07/2016)
#>
########################[Initial Validation]################################
	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )	,
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSV =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($Global | Test-Path)) {
		# file with path $path doesn't exist
		Write-Host "$Global doesnot exist. Kindly retry with correct file" -foregroundcolor red
		Write-Host
		exit
	}
	if (-not ($InputCSV | Test-Path)) {
		# file with path $path doesn't exist
		Write-Host "$InputCSV doesnot exist. Kindly retry with correct file" -foregroundcolor red
		Write-Host
		exit
	}

###################[Initialization]####################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	# Create a Log file in the current dir
	$LogPath = "Cluster"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	
###################[Declaring Input ###################################
	$Error.Clear()
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$GlobalConfig = Import-Csv -Path $Global
	$InputCSV = Import-Csv -Path $InputCSV
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:VCUser = $null
	$global:VCPassword = $null
	
###########################[Main Function]##############################
	ForEach ($Configvalue in $GlobalConfig) {
		if ($Configvalue.Config -eq "vCenter IP"){
				$global:vCenter =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "vCenter Administrator"){
				$global:VCUser =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "vCenter Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Datacenter Name"){
				$global:Datacenter =$Configvalue.Value.trim()
			}
	}
###########################[Check Input Values]##############################
	if($global:vCenter -eq ""){
		Write-host"vCenter IP Filed is Empty"
		Exit
	}
	if($global:VCUser -eq ""){
		Write-host"vCenter Administrator Filed is Empty"
		Exit
	}
	if($global:VCPassword -eq ""){
		Write-host"vCenter Password Filed is Empty"
		Exit
	}
	if($global:Datacenter -eq ""){
		Write-host"Datacenter Name Filed is Empty"
		Exit
	}

###########################[Connect-VIServer]##############################	
	Disconnect-VIServer * -Confirm:$false
	Add-Content -Path $LogPath -Value "CLUSTER LOG FILE"
	$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
	If ($results -ne $null){
		Write-Output "[Server $global:vCenter] Connected"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
	}
	else{
		Write-Error "[Server-$global:vCenter] ERROR: Unable to connect"
		Write-Warning "Script execution halted `n"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}

######################[Cluster Configuration]###############################
	foreach($Cluster in $InputCSV){	
		$Clustervalue=$Cluster.Cluster
		$DRS=$Cluster.DRS
		$DRSAutomationLevel=$Cluster.AutomationLevel
		$DRSMigthreshold=$Cluster.MigrationThreshold
		$HA=$Cluster.HighAvailability
		$VMMonitoring=$Cluster.VMMonitoring
		$HostMonitoring=$Cluster.HostMonitoring
		$HostIsolation=$Cluster.HostIsolation
		$AdmissionControl=$Cluster.AdmissionControl
		$RestartPriority=$Cluster.RestartPriority
		$EVCMode=$Cluster.EVCMode
		$EVCModeTye=$Cluster.EVCModeTye
		$SwapFilePolicy=$cluster.SwapFilePolicy
			
		$datacenterName=Get-Datacenter -Name $global:Datacenter
		$clusterName=Get-Cluster -Name $Clustervalue -Location $datacenterName 
		If($clusterName -ne $null){
			#DRS
			If($DRS -eq "Yes"){
				Add-Content -Path $LogPath -Value ""
				Write-Host
				Write-Output "[CLUSTER-$clusterName]"
				Add-Content -Path $LogPath -Value "[CLUSTER-$clusterName]"
				Write-Output "[CLUSTER-$clusterName] Enabling DRS and Features "
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[CLUSTER-$clusterName] Enabling DRS and Features"
				
				$SetCluster=Set-Cluster -Cluster $ClusterName  -DRSEnabled:$true -DRSAutomationLevel $DRSAutomationLevel -Confirm:$false
				If($DRSMigthreshold -eq "Priority 1"){
					$rate=5
				}
				elseif($DRSMigthreshold -eq "Priority 2"){
					$rate=4
				}
				elseif($DRSMigthreshold -eq "Priority 3"){
					$rate=3
				}
				elseif($DRSMigthreshold -eq "Priority 4"){
					$rate=2
				}
				elseif($DRSMigthreshold -eq "Priority 5"){
					$rate=1
				}
				$clus=Get-Cluster -Name $ClusterName | Get-view
				$clusSpec = New-Object VMware.Vim.ClusterConfigSpecEx
				$clusSpec.drsConfig = New-Object VMware.Vim.ClusterDrsConfigInfo
				$clusSPec.drsConfig.vmotionRate = $rate
				$Reconfig=$clus.ReconfigureComputeResource_Task($clusSpec, $true)
				Write-Output "[CLUSTER-$clusterName] Updated DRS Migration thershold $DRSMigthreshold"
				Add-Content -Path $LogPath -Value "DateTime]::Now)]-[CLUSTER-$clusterName] Updated DRS Migration thershold $DRSMigthreshold"
			}
			#HA
			If($HA -eq "Yes" ){
				Write-Output "[CLUSTER-$clusterName] Enabling HA and features"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[CLUSTER-$clusterName] Enabling HA and features"
				
				$SetCluster=Set-Cluster -Cluster $ClusterName  -HAEnabled:$true -HAIsolationResponse $HostIsolation -HAAdmissionControlEnabled:$true -HARestartPriority $RestartPriority -Confirm:$false
				If($VMMonitoring -eq "Disabled"){
					$VMMonitor="vmMonitoringDisabled"
				}
				elseif($VMMonitoring -eq "VM Monitoring Only"){
					$VMMonitor="vmMonitoringOnly"
				}
				elseif($VMMonitoring -eq "VM and Application Monitoring"){
					$VMMonitor="vmMonitoringOnly"
				}
				If($HostMonitoring -eq "Yes"){
					$HostMonitor="enabled"
				}
				else{
					$HostMonitor="disabled"
				}
				Write-Output "[CLUSTER-$clusterName] Updating vmMonitoring Settings-$VMMonitor and HostMonitoring Settings-$HostMonitor"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[CLUSTER-$clusterName] Updating vmMonitoring Settings-$VMMonitor and HostMonitoring Settings-$HostMonitor"
				$clus=Get-Cluster -Name $ClusterName | Get-view
				$clusSpec = New-Object VMware.Vim.ClusterConfigSpecEx
				$clusSpec.DasConfig = New-Object VMware.Vim.ClusterDasConfigInfo
				$clusSpec.DasConfig.vmMonitoring =$VMMonitor
				$clusSpec.DasConfig.hostMonitoring =$HostMonitor
				$Reconfig=$clus.ReconfigureComputeResource_Task($clusSpec, $true)
			}
			#EVC 
			if($EVCMode -ne ""){
				If($EVCMode -ne "Disable EVC"){
					$SetCluster=Set-Cluster -Cluster $ClusterName -EVCMode $EVCModeTye -Confirm:$false
					Write-Output "[CLUSTER-$clusterName] Enabling EVCMode-$EVCMode and Mode type-$EVCModeTye"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[CLUSTER-$clusterName] Enabling EVCMode-$EVCMode and Mode type-$EVCModeTye"
				}
				else{
					if($clusterName.EVCMode -ne $null){
						$SetCluster=Set-Cluster -Cluster $ClusterName -EVCMode $null
						Write-Output "[CLUSTER-$clusterName] EVCMode is disabled"
						Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[CLUSTER-$clusterName] EVCMode is disabled"
					}
					else{
						Write-Output "[CLUSTER-$clusterName] EVCMode already disabled"
						Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[CLUSTER-$clusterName] EVCMode already disabled"
					}
				}
			}
			else{
				Write-Host"EVCMode input value is empty"
			}
			#Swap File 	 
			if($SwapFilePolicy -ne ""){
				If($SwapFilePolicy -eq "Same Directory as the Virtual Machine"){
					$SwapFile="WithVM"
				}
				else{
					$SwapFile="InHostDatastore"
				}
				$GetSwap=$clusterName.VMSwapfilePolicy
				if($GetSwap -ne $SwapFile){
					$SetCluster=Set-Cluster -Cluster test -VMSwapfilePolicy $SwapFile
					Write-Output "[CLUSTER-$clusterName] Enabling SwapfilePolicy-$SwapFilePolicy"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[CLUSTER-$clusterName] Enabling SwapfilePolicy-$SwapFilePolicy"
				}
				else{
					Write-Output "[CLUSTER-$clusterName] SwapfilePolicy already Enabled"
					Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[CLUSTER-$clusterName] SwapfilePolicy already Enabled"
				}
			}			 
		}			
	}