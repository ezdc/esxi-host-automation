<#
.SYNOPSIS
	Enable iSCSI software adapter 

.DESCRIPTION
	Enable iSCSI software adapter,attche send targets IP Address,add vmotion network for storage and rescan all HBAs and VMFS for the Host

.EXAMPLE
.	\4_iSCSI.ps1 -Global -Global.CSV -InputCSV 2_HostConfig.CSV
	2_HostConfig.CSV- Input file for all the Host details
	Global.CSV- vCenter Details and common details

.NOTES
	File Name : 4_iSCSI.ps1
	Author    : Raj Srinivasan 
	Requires  : PowerCLI-6.0
	Version	  : 1.0(19/07/2016)
#>
########################[Initial Validation]################################
[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$Global =  $(throw "-CSV parameter is required. This is the input csv file." )	,
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSV =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
if (-not ($Global | Test-Path)) {
		# file with path $path doesn't exist
		Write-Host "$Global doesnot exist. Kindly retry with correct file" -foregroundcolor red
		Write-Host
		exit
	}
if (-not ($InputCSVFile | Test-Path)) {
		# file with path $path doesn't exist
		Write-Host "$InputCSV doesnot exist. Kindly retry with correct file" -foregroundcolor red
		Write-Host
		exit
	}

###################[Initialization]####################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	# Create a Log file in the current dir
	$LogPath = "iSCSI"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	
###################[Declaring Input]###########################
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	
	$GlobalConfig = Import-Csv -Path $Global
	$InputCSV = Import-Csv -Path $InputCSV
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:VCUser = $null
	$global:VCPassword = $null
	$global:ISCSIIP=$null
###########################[Getting vCenter Details]########################

	ForEach ($Configvalue in $GlobalConfig) {
		if ($Configvalue.Config -eq "vCenter IP"){
				$global:vCenter =$Configvalue.Value
			}
		Elseif ($Configvalue.Config -eq "vCenter Administrator"){
				$global:VCUser =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "vCenter Password"){
				$global:VCPassword =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "Datacenter Name"){
				$global:Datacenter =$Configvalue.Value.trim()
			}
		Elseif ($Configvalue.Config -eq "ISCSIIP"){
				$global:ISCSIIP =$Configvalue.Value.trim()
			}
	}
###########################[Check Input Values]##############################
	if($global:vCenter -eq ""){
		Write-host"vCenter IP Filed is Empty"
		Exit
	}
	if($global:VCUser -eq ""){
		Write-host"vCenter Administrator Filed is Empty"
		Exit
	}
	if($global:VCPassword -eq ""){
		Write-host"vCenter Password Filed is Empty"
		Exit
	}
	if($global:Datacenter -eq ""){
		Write-host"Datacenter Name Filed is Empty"
		Exit
	}
	if($global:ISCSIIP -eq ""){
		Write-host"ISCSIIP Name Filed is Empty"
		Exit
	}
###########################[Connect-VIServer]##############################	
	Disconnect-VIServer * -Confirm:$false
	Add-Content -Path $LogPath -Value "ISCSI LOG FILE"
	$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
	If ($results -ne $null){
		Write-Output "[Server $global:vCenter] Connected"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
	}
	else{
		Write-Error "[Server-$global:vCenter] ERROR: Unable to connect"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}
###########################[Main Function]##############################	
	foreach($iSCSI in $InputCSV){
		Write-Host
		Add-Content -Path $LogPath -Value ""
		$Hostname=$iSCSI.IP
		if($Hostname -ne "")
			$datacenterName=Get-Datacenter -Name $global:Datacenter
			$HostIP=Get-VMHost -Name $Hostname -Location $datacenterName
			$Enable=Get-VMHostStorage $HostIP | Set-VMHostStorage -SoftwareIScsiEnabled $True  -WarningAction silentlycontinue | Out-Null	
			$iSCSI_HBA = Get-VMHost -Name $HostIP | Get-VMHostHba | Where {$_.Type -eq "Iscsi"} | Where {$_.Model -eq "iSCSI Software Adapter"}
			Write-Output "[HOST-$Hostname] iSCSI Software Adapter-Enabled"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$Hostname iSCSI Software Adapter-Enabled"
			$esxcli = Get-EsxCli -VMhost $HostIP -WarningAction silentlycontinue -ErrorAction SilentlyContinue | Out-Null
			$Esxcli.iscsi.networkportal.add($iSCSI_HBA, $Null, "vmk1") 
			Write-Output "[HOST-$Hostname] Configured VMkernel Port-vmk1"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$Hostname] Configured VMkernel Port-vmk0"
			$Addtarget=Get-VMHostHba $iSCSI_HBA -VMhost $HostIP | New-IScsiHbaTarget -Address $global:ISCSIIP -WarningAction silentlycontinue | Out-Null
			Get-VMHostStorage -VMHost $HostIP -RescanAllHba -RescanVmfs
			Write-Output "[HOST-$Hostname] Added Sent Targets Address-$global:ISCSIIP"
			Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[HOST-$Hostname] Added Sent Targets Address-$global:ISCSIIP"
		}
		else{
			Write-Host "IP Input is Empty"
		}
	}