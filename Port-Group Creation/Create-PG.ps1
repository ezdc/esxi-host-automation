<#
.SYNOPSIS
Create PortGroup

.DESCRIPTION
Create portgroup for all distributed switch

.EXAMPLE
 .\Create-PG.ps1 .\Create-PG.csv
Create-PG.csv-vCenter and portgroup Input file

.NOTES
File Name : Create-PG.ps1
Author    : Raj Srinivasan 
Requires  : PowerCLI 
Version	  : 6.0
#>
###################[Inputfile Validation]####################################
 	[CmdletBinding(SupportsShouldProcess=$true)]
	Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$InputCSVFile =  $(throw "-CSV parameter is required. This is the input csv file." )
	)
	if (-not ($InputCSVFile | Test-Path)) {
    # file with path $path doesn't exist
	Write-Output "$InputCSVFile doesnot exist. Kindly retry with correct file" -foregroundcolor red
	Write-Host
	exit
	}

###################[Initialization]####################################
	clear
	#Set Error Action to Silently Continue
	$ErrorActionPreference = "SilentlyContinue"
	
	# Create a Log file in the current dir
	$LogPath = "PortGroup"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	
###################[Declaring Input]###########################
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null){
		Add-PSSnapin VMware.VimAutomation.Core
		if ($error.Count -eq 0){
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
		else{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else{
		Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$GlobalConfig = Import-Csv -Path $vCenterFilepath
	$InputCSV = Import-Csv -Path $InputCSVFile 
	$global:vCenter = $null
	$global:Datacenter = $null
	$global:VCUser = $null
	$global:VCPassword = $null
	
###########################[Connect vCenter]###############################

	$global:vCenter=$InputCSV[0].VCIP
	$global:VCUser=$InputCSV[0].VCAdministrator
	$global:VCPassword=$InputCSV[0].VCPassword
	$global:Datacenter=$InputCSV[0].Datacenter
	
	Disconnect-VIServer * -Confirm:$false
	Add-Content -Path $LogPath -Value "PORTGROUP LOG FILE"
	$results = Connect-VIServer -Server $global:vCenter -user $global:VCUser -Password $global:VCPassword -WarningAction silentlycontinue
	If ($results -ne $null){
		Write-Output "[Server $global:vCenter] Connected"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter] Connected"
	}
	else{
		Write-Error "[Server-$global:vCenter] ERROR: Unable to connect"
		Write-Warning "Script execution halted `n"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[Server-$global:vCenter]ERROR: Unable to connect to the vCenter Server"
		exit
	}
	
######################[Main Function]###############################
	foreach($Network in $InputCSV)
	{
		$Switch=$Network.SwitchName
		$Portgroup=$Network.Portgroup
		$vlanID=$Network.VLAN
		$datacenterName=Get-Datacenter -Name $global:Datacenter
		
		Add-Content -Path $LogPath -Value ""	
		Write-Host
		Write-Output "[vDSwitch-$Switch]"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch]"
		$vDS=Get-VDSwitch -Name $Switch -Location $datacenterName
		Write-Output "[vDSwitch-$Switch] Getting switch details"
		Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch] Getting switch details"
		if($vDS -ne $null){
			$PG=New-VDPortgroup -Name $Portgroup -VlanId $vlanID -VDSwitch $vDS
			if($PG -ne $null){
				Write-Output "[vDSwitch-$Switch]-[PortGroup-$Portgroup] Created"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)]-[vDSwitch-$Switch]-[PortGroup-$Portgroup] Created"
				}
			else{
				Write-Warning "[vDSwitch-$Switch]-[PortGroup-$Portgroup] Already exists"
				Add-Content -Path $LogPath -Value "[$([DateTime]::Now)] [vDSwitch-$Switch]-[PortGroup-$Portgroup]  Already exists"
			}
		}
		else{
			Write-Warning "[vDSwitch-$Switch] No such name exists"
			Add-Content -Path $LogPath -Value $Logvalue "[$([DateTime]::Now)]-[vDSwitch-$Switch] No such name exists"
		}
	}
	Write-Host "******************************Completed************************************"
	