<#
.SYNOPSIS
Custom ESXi ISO with the required kickstart configurations (USB -> USB Install)

.DESCRIPTION
Create a custom ESXi ISO with the required kickstart configurations based on the inout CSV file.
Note: This ISO is ment for USB to USB installations.

.EXAMPLE
./CreateISO.ps1 -CSV myhosts.csv -ISO VMware-6.0.iso
myhosts.csv    - CSV file with all the host details
VMware-6.0.iso - Base ESXi 6.0 ISO image file

.EXAMPLE
./CreateISO.ps1 -CSV myhosts.csv -ISO VMware-6.0.iso -Verbose
Shows verbose text output.

.NOTES
File Name : CreateISO.ps1
Author    : Raj Srinivasan and Fahad Khan
Requires  : PowerShell V5 Windows 2012 Server
Version		: 0.6
#>

[CmdletBinding(SupportsShouldProcess=$true)]
Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$CSV =  $(throw "-CSV parameter is required. This is the input csv file with all the host information."),
		[string]$ISO = $(throw "-ISO parameter is required. This is the ESXi Base ISO file.")
)

function Main () {
	# Check if the input CSV exists!
	if (-not ($CSV | Test-Path)) {
		Write-Output "CSV file - $CSV does not exist. Kindly retry with correct input file."
		return
	}

	# Get the current working directory path
	$global:BasePath = (Get-Item -Path ".\" -Verbose).FullName
	$global:DestinationPath = "$global:BasePath"

	# Create a Log file in the current dir
	$LogPath = "ISOLog-"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}

	$Error.Clear()

	# Extract the ISO
	ExtractISO

	# Create all the kickstart files
	Write-Verbose "Create the KS directory inside the copied image directory."
	$global:KSPath = ($global:DestinationPath + "\KS").ToUpper()
	if(!(Test-Path -Path $global:KSPath)) {
		New-Item -ItemType Directory -Force -Path $global:KSPath | Out-Null
	} else {
		if (Test-Path $global:KSPath\* -include *.cfg) {
			Write-Warning "Multilple kickstart congiuration files already exists!`nClean the directory and re-run the script!"
			exit
		}
	}

	# Remove the existing boot menu file - This is the default file that will exist
	if (Test-Path -Path $global:DestinationPath\ISOLINUX.CFG) {
		Write-Warning "Boot Menu (ISOLINUX.CFG) exists ... Deleting!"
		Remove-Item $global:DestinationPath\ISOLINUX.CFG -Force
	}

	# Create the header for the boot menu (ISOLINUX.CFG)
	$string = "DEFAULT menu.c32`n"
	$string += "prompt 0`n"
	$string += "menu title CUSTOM ESXi 6.0 INSTALL`n"
	$string += "timeout 0"
	Write-Output "Updating the new Boot Menu (ISOLINUX.CFG) ..."
	Add-Content -Path $global:DestinationPath\ISOLINUX.CFG -Value $string

	$counter = 1
	$label = ""
	$AllHosts = Import-CSV $CSV
	ForEach ($InputCSV in $AllHosts) {
		$Hostname = $InputCSV.Hostname
		$IP = $InputCSV.IP
		$VLAN = $InputCSV.VLAN
		$Gateway = $InputCSV.Gateway
		$Subnet = $InputCSV.Subnet
		$DNS = $InputCSV.DNS
		$MAC = $InputCSV.MAC

		# Create KS Files
		$KSFile =("KS_"+$Hostname+".CFG").ToUpper()
		Write-Verbose "$KSFile, $Hostname, $IP, $VLAN, $Gateway, $DNS, $MAC"
		Write-Output "Creating the kickstart file - $KSFile"
		Create-KSFile $KSFile $Hostname $IP $VLAN $Gateway $Subnet $DNS $MAC

		# Create the Boot Menu file
		if ($counter -le 9) { $label = "0" + $counter }
		Modifying-ISOLinux $counter $label $Hostname $IP $KSFile
		$counter += 1
	}

	# Create the new ISO image
	Write-Warning "Existing Custom ISO will be deleted from the `"newiso`" directory ..."
	if(!(Test-Path -Path $global:BasePath\newiso)) {
		New-Item -ItemType Directory -Force -Path $global:BasePath\newiso | Out-Null
	} else {
		Remove-Item $global:BasePath\newiso\* -Force
	}
	Write-Output "Creating the new custom ISO file ..."
	Create-ISO
}

function ExtractISO () {

	# Check for the source ISO file
	If ((Test-Path "$ISO") -eq $False) {
		Write-Warning "ISO File missing!`nEnsure that you copy the vSphere ISO image into the current directory - $global:BasePath!"
		Add-Content -Path $LogPath -Value "ISO File missing!"
		Add-Content -Path $LogPath -Value "Ensure that you copy the vSphere ISO image into the current directory - $global:BasePath!"
		exit
	}

	# Get the ISO metadata and extract to a local directory
	$iso = Get-Item $ISO
	$BaseName = $iso.BaseName
	$FullName = $iso.FullName

	Write-Output "Ensure that the ESXi Image is not already mounted!`nPress any key to continue ..."
	$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")

	Write-Output "Mount Image $BaseName"
	Mount-DiskImage $FullName
	$CDDriveLetter = "ERROR"

	get-psdrive | where {$_.Description -match "ESXI-5" -or $_.Description -match "ESXI-6" } | foreach-object {
		$CDDriveLetter = $_.root
	}
	Write-Output "Drive is $CDDriveLetter"

	$SourcePath = $CDDriveLetter + "\*"
	$DestinationPath = $global:BasePath + "\iso-" + (get-date).ToString("yyyyMMddHHmmss")

	# Create the Directory to copy the ISO image
	New-Item -ItemType Directory -Force -Path $DestinationPath | Out-Null

	# Copy the image contents into a new directory
	Copy-Item $SourcePath $DestinationPath
	Write-Output "Will unmount $FullName ..."
	Dismount-DiskImage -ImagePath $FullName
	Write-Output "Image $BaseName unmounted."

	$global:DestinationPath = $DestinationPath
	Write-Verbose "Image copied into $global:DestinationPath"
}

# Create the kickstart files for all the hosts
function Create-KSFile($KSFile,$Hostname,$IP,$VLAN,$Gateway,$Subnet,$DNS,$MAC) {

	$strfile1 =""
	$path = $global:KSPath
	$filepath = $global:KSPath + "\" + $KSFile
	$DNS = $DNS -replace '\s',''
	if ($MAC -eq "") { $MAC = "vmnic0"}

	Write-Verbose "Kickstart Values are $Hostname,$IP,$VLAN,$Gateway,$Subnet,$DNS,$MAC"

	if((Get-Item $path) -is [System.IO.DirectoryInfo])
	{
		try {
			$strfile1 = "#!/bin/bash`n`n"
			$strfile1 += "accepteula`n"
			$strfile1 += "install --firstdisk=usb-storage --overwritevmfs`n"
			$strfile1 += "rootpw vmware123`n"
			$strfile1 += "reboot`n`n"
			$strfile1 += "%include /tmp/networkconfig`n`n"
			$strfile1 += "%pre --interpreter=busybox`n`n"
			$strfile1 +="#extract network info from bootup`n"
			$strfile1 += "echo ""network --bootproto=static --addvmportgroup=true --device=$MAC --ip=$IP --netmask=$Subnet --gateway=$Gateway --vlanid=$VLAN  --nameserver=$DNS --hostname=$Hostname""  > /tmp/networkconfig`n`n"
			$strfile1 += "%firstboot --interpreter=busybox`n`n"
			$strfile1 += "ks_key = ""$Hostname-KS""`n`n"
			$strfile1 += "logger $ks_key "" Enabling & Starting SSH""`n"
			$strfile1 += "vim-cmd hostsvc/enable_ssh`n"
			$strfile1 += "vim-cmd hostsvc/start_ssh`n`n"
			$strfile1 += "logger $ks_key "" Enabling & Starting ESXi Shell""`n"
			$strfile1 += "vim-cmd hostsvc/enable_esx_shell`n"
			$strfile1 += "vim-cmd hostsvc/start_esx_shell`n`n"
			$strfile1 += "logger $ks_key "" Suppressing ESXi Shell Warning""`n"
			$strfile1 += "esxcli system settings advanced set -o /UserVars/SuppressShellWarning -i 1`n`n"
			$strfile1 += "logger $ks_key "" ESXi Shell interactive idle time logout""`n"
			$strfile1 += "esxcli system settings advanced set -o /UserVars/ESXiShellInteractiveTimeOut -i 3600`n`n"
			$strfile1 += "# Rename local datastore to something more meaningful`n"
			$strfile1 += "vim-cmd hostsvc/datastore/rename datastore1 ""`$(hostname -s)-local-storage-1""`n`n"
			$strfile1 += "# Disable IPv6 for VMkernel interfaces`n"
			$strfile1 += "esxcli system module parameters set -m tcpip4 -p ipv6=0`n`n"
			$strfile1 += "# Needed for configuration changes that could not be performed in esxcli`n"
			$strfile1 += "esxcli system shutdown reboot -d 60 -r ""rebooting after host configurations""`n"
			Add-Content -Path $filepath -Value $strfile1

			$logvalue = "Successfully created new KS file: " + $filepath
			Add-Content -Path $LogPath -Value $logvalue
		}
		Catch [System.Exception] {
			$logvalue = "Failed to create new file: " + $file
			Add-Content -Path $LogPath -Value $logvalue
		}
	}
}

# Boot Menu - ISOLINUX.CFG
function Modifying-ISOLinux($counter,$label,$Hostname,$IP,$KSFile) {

	$string = "LABEL ESX$label - $Hostname`n"
	$string += " menu LABEL ^$counter - $Hostname - $IP`n"
	$string += " kernel /MBOOT.C32`n"
	$string += " append initrd=/ubninit -c boot.cfg ks=usb:/KS/$KSFile +++"

	Add-Content -Path $global:DestinationPath\ISOLINUX.CFG -Value $string
	$logvalue = "Successfully Modifying-ISOLinux for $Hostname"
	Add-Content -Path $LogPath -Value $logvalue
}

# Create the new ISO in the destination directory (.\newiso\)
function Create-ISO () {
	cd $global:DestinationPath
	Get-ChildItem -Path $global:DestinationPath -Filter ISOLINUX.BIN | foreach { $_.Attributes = 'Normal' }
	cmd.exe /c "$global:BasePath\cdrtools\mkisofs.exe -quiet -l -no-iso-translate -sysid '' -A esxi6 -V esxi6 -c BOOT.CAT -b ISOLINUX.BIN -no-emul-boot -boot-load-size 4 -boot-info-table -input-charset utf-8 -o $global:BasePath\newiso\new_esxi6.iso ."
	cd $global:BasePath
}

# Main
clear
Main
