!include "LogicLib.nsh"
!include "EnvVarUpdate.nsh"
Name "CDR Tools"
Icon "logo.ico"
OutFile "CDR-Tools.exe"

Page license
Page instfiles

LicenseText "Please read carefully. You must agree to this EULA before installing." 
LicenseData "gpl.rtf"

InstallDir $PROGRAMFILES\cdrtools
;Installs neccessary files for BEeN GRUBed to work. 
Section "cdrtools"

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  File align_test.exe
  File AN-3.00.txt
  File avoffset.exe
  File btcflash.exe
  File cdda2wav.exe
  File rscsi.exe
  File readcd.exe
  File mkisofs.exe
  File isovfy.exe
  File isoinfo.exe
  File isodump.exe
  File isodebug.exe
  File devdump.exe
  File cdrecord.exe
  File cygwin1.dll
  File cygintl-8.dll
  File cygiconv-2.dll
  File scgskeleton.exe
  File scgcheck.exe

	${EnvVarUpdate} $0 "PATH" "A" "HKLM" "C:\Program Files\cdrtools"
SectionEnd