<#

.SYNOPSIS
This is a Powershell script to configure ESXi Host, after clean installation.
 
.DESCRIPTION
The script, assumes that it is a clean installation of ESXi, that the root password is set and a static IP is configured.
When given a ESXi Host IP and a ESXi Hostname will configure the following:

* Add ESXi Host
* Start NTP service
* DNS
* Change root password
* Create new service account
* Host in maintenance mode


.EXAMPLE
PS C:\> ESXCFG.ps1 "Global host configuration.csv" "hostconfiguration.csv"
Shows verbose text output.

.NOTES
File Name : ESXCFG.ps1
Author    : Raj Srinivasan
Requires  : PowerShell V5 Windows 2012 Server
Version		: 0.1

Link
http://www.ezdcautomation.com/


#>

# Input Paramters
[CmdletBinding(SupportsShouldProcess=$true)]
Param (
		[Parameter(Mandatory=$True)]
		[ValidateNotNull()]
		[string]$G_CSV =  $(throw "-CSV parameter is required. This is the input csv file with all the Global host configuration."),
		[string]$Host_CSV = $(throw "-CSV parameter is required. This is the input csv file with all the host information.")
)

				# Start Log File
				$timestamp = Get-Date -f "MM-dd-yyyy-HH.mm.ss"
				$LogFile = "ESXiCFG"+$ESXHostName+"-"+$timestamp+".log"
				Start-Transcript -Path $LogFile
				
				
# ESXi Variables
$global:ESXiUserName = $null
$global:ESXiPassword = $null
$global:ServiceAccount = $null
$global:ServiceAccountPassword = $null
$global:DNS = $null
$global:DNSSuffixes = $null
$global:NTP = $null
$global:SYSlog = $null
$global:DomainName = $null
$global:DomainUsername = $null
$global:DomainPasssword = $null
$global:vCenter = $null
$global:Datacenter = $null
$global:Newrootpassword = $null

#Main Function for ESXi Configuration.
function Main () {
	# Check if the input CSV exists!
	if (-not ($G_CSV | Test-Path)) {
		Write-Output "CSV file - $CSV does not exist. Kindly retry with correct input file."
		return
	}
	if (-not ($Host_CSV | Test-Path)) {
		Write-Output "CSV file - $CSV does not exist. Kindly retry with correct input file."
		return
	}
	
	# Get the current working directory path
	$global:BasePath = (Get-Item -Path ".\" -Verbose).FullName
	$global:DestinationPath = "$global:BasePath"
	# Create a Log file in the current dir
	$LogPath = "ESXCFGISOLog-"+(get-date).ToString("yyyyMMddHHmmss")+".log"
	if (!(Test-Path $LogPath)) {
		Write-Verbose "Creating log file - $LogPath."
		$NewLogFile = New-Item $LogPath -Force -ItemType File
	}
	$Error.Clear()
	$vmsnapin = Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue
	$Error.Clear()
	if ($vmsnapin -eq $null)
	{
		Add-PSSnapin VMware.VimAutomation.Core
	if ($error.Count -eq 0)
		{
			write-host "PowerCLI VimAutomation.Core Snap-in was successfully enabled." -ForegroundColor Green
		}
	else
		{
			write-host "ERROR: Could not enable PowerCLI VimAutomation.Core Snap-in, exiting script" -ForegroundColor Red
			Exit
		}
	}
	else
	{
			Write-Host "PowerCLI VimAutomation.Core Snap-in is already enabled" -ForegroundColor Green
	}
	$Error.Clear()
	$GlobalConfig = Import-CSV $G_CSV
	$HostConfig = Import-CSV $Host_CSV
	
	ForEach ($Configvalue in $GlobalConfig) {
			#Write-Host $Configvalue.Config.trim()
			if($Configvalue.Config -eq "ESXi UserName"){
						$global:ESXiUserName =$Configvalue.Value
						Write-Host $global:ESXiUserName.trim()
						}
			Elseif ($Configvalue.Config -eq "ESXi Password"){
					$global:ESXiPassword =$Configvalue.Value
					Write-Host $global:ESXiPassword.trim()
			}
			Elseif ($Configvalue.Config.trim() -eq "Service Account"){
					$global:ServiceAccount =$Configvalue.Value.trim()
					Write-Host $global:ServiceAccount.trim()
					
			}
			Elseif ($Configvalue.Config.trim() -eq "Service Account Password"){
					$global:ServiceAccountPassword =$Configvalue.Value.trim()
					Write-Host $global:ServiceAccountPassword.trim()
			}
			Elseif ($Configvalue.Config -eq "DNS"){
					$global:DNS =$Configvalue.Value.trim()
					Write-Host $global:DNS
			}
			Elseif ($Configvalue.Config -eq "DNS Suffixes"){
					$global:DNSSuffixes =$Configvalue.Value
					Write-Host $global:DNSSuffixes
			}
			Elseif ($Configvalue.Config.trim() -eq "NTP"){
					$global:NTP =$Configvalue.Value.trim()
					Write-Host $global:NTP
			}
			Elseif ($Configvalue.Config -eq "SYSlog"){
					$global:SYSlog =$Configvalue.Value
					Write-Host $global:SYSlog
			}
			Elseif ($Configvalue.Config -eq "Domain Name"){
					$global:DomainName =$Configvalue.Value
					Write-Host $global:DomainName
			}
			Elseif ($Configvalue.Config -eq "Domain UserName"){
					$global:DomainUsername =$Configvalue.Value
					Write-Host $global:DomainUsername
			}
			Elseif ($Configvalue.Config -eq "Domain Passsword"){
					$global:DomainPasssword =$Configvalue.Value
					Write-Host $global:DomainPasssword
			}
			Elseif ($Configvalue.Config -eq "vCenter"){
					$global:vCenter =$Configvalue.Value
					Write-Host $global:vCenter
			}
			Elseif ($Configvalue.Config -eq "Datacenter Name"){
					$global:Datacenter =$Configvalue.Value.trim()
					Write-Host $global:Datacenter
					
			}
			Elseif ($Configvalue.Config -eq "New Root Password"){
					$global:Newrootpassword =$Configvalue.Value.trim()
					Write-Host $global:Newrootpassword
					
			}
						
		}
			
		ForEach ($Hostconfigvalue in $HostConfig) {
			$Clustername = $Hostconfigvalue.Cluster.trim()
			$IP = $Hostconfigvalue.IP
			$ESXHostName = $Hostconfigvalue.Hostname.trim()
			
			if($global:vCenter){
						
				$Logvalue = "`n*****************************  $ESXHostName  *********************************"
				Add-Content -Path $LogPath -Value $Logvalue
				Write-Host "`n Connecting to vCenter Server $global:vCenter...`n"
				$results = Connect-VIServer -Server $global:vCenter -WarningAction silentlycontinue
			
				If ($results -eq $null){
					Write-Host "ERROR: Unable to connect to the vCenter Server"
					Write-Host "Script execution halted `n"
					$Logvalue = " ERROR: Unable to connect to the vCenter Server"
					Add-Content -Path $LogPath -Value $Logvalue
					exit
				}
				else{
					# Adds host to datacenter, login with root credentials
					Write-Host "`n Adding ESXi Host $ESXHostName to vCenter Server"
					try{
						$VMHost = Get-VMHost -Name $IP -Location (Get-Datacenter -Name $global:Datacenter | Get-Cluster -Name $Clustername) -ErrorAction SilentlyContinue
						if($VMHost -eq $null)
						{
							Add-VMHost -Name $IP -Location (Get-Datacenter -Name $global:Datacenter | Get-Cluster -Name $Clustername) -User $global:ESXiUserName -Password $global:ESXiPassword -ErrorAction SilentlyContinue | Out-Null
							$Logvalue = " Adding ESXi Host $ESXHostName to vCenter Server"
							Add-Content -Path $LogPath -Value $Logvalue
						}
						else
						{
							Write-Host " ESXi Host $ESXHostName Already Exist."  -ForegroundColor Yellow
							$Logvalue = " ESXi Host $ESXHostName Already Exist."
							Add-Content -Path $LogPath -Value $Logvalue
						
						}
					}
					catch{
							Add-VMHost -Name $IP -Location (Get-Datacenter -Name $global:Datacenter | Get-Cluster -Name $Clustername) -User $global:ESXiUserName -Password $global:ESXiPassword -ErrorAction SilentlyContinue | Out-Null
							$Logvalue = " Adding ESXi server $ESXHostName to vCenter Server"
							Add-Content -Path $LogPath -Value $Logvalue
					}
															
				}
				
				#Disconnect from vCenter Server
				Write-Host "`n Disconecting from vCenter Server $global:vCenter. `n"
				Disconnect-VIServer -Server $global:vCenter -Confirm:$False
				$Logvalue = " Disconecting from vCenter Server $global:vCenter."
				Add-Content -Path $LogPath -Value $Logvalue
				
				# Connect to ESXi server
				Write-Host "`n Connecting to ESXi Host $IP... `n"
				Connect-VIServer $IP -username $global:ESXiUserName -Password $global:ESXiPassword -WarningAction silentlycontinue | Out-Null
				$Logvalue = " Connecting to ESXi Host $IP..."
				Add-Content -Path $LogPath -Value $Logvalue
				
				# Set DNS Servers
				Write-Host "`n Configuring DNS Server... `n"  -ForegroundColor Yellow
				$Logvalue = " Configuring DNS Server..."
				Add-Content -Path $LogPath -Value $Logvalue
				$DNS = $global:DNS.Split(',')
				$DNSVal = Get-VMHostNetwork -VMHost $IP | Set-VMHostNetwork -DomainName $global:DomainName -DNSAddress $DNS[0],$DNS[1] -Confirm:$false
				Write-Host " Completed Configuring DNS servers $global:DNS."  -ForegroundColor Green
				$Logvalue = " Completed Configuring DNS servers $global:DNS."
				Add-Content -Path $LogPath -Value $Logvalue
				
				# Add NTP Server
				Write-Host "`n Configuring NTP server...`n" -ForegroundColor Green
				$Logvalue = " Configuring NTP server..."
				Add-Content -Path $LogPath -Value $Logvalue
				try { 
						$NTPServer = Get-VMHostNtpServer -VMHost $IP  -ErrorAction silentlycontinue; 
						if ($NTPServer -eq $null) { 
							$NTP = Add-VMHostNtpServer -NtpServer $global:NTP -VMHost $IP -Confirm:$false				
							Write-Host " Completed Configuring NTP servers $global:NTP"  -ForegroundColor Green
							$Logvalue = " Completed Configuring NTP servers $global:NTP"
							Add-Content -Path $LogPath -Value $Logvalue
						}
						else { 
								Write-Host " The NtpServer '$global:NTP' already exist on VMHost $IP" -ForegroundColor Yellow
								$Logvalue = " The NtpServer '$global:NTP' already exist on VMHost $IP..."
								Add-Content -Path $LogPath -Value $Logvalue
						}
					} 
				catch { 
						
				}
				
				# Start NTP Daemon
				Write-Host "`n Starting NTP Daemon...`n"  -ForegroundColor Yellow
				$ntpclient = Get-VMHostService -VMHost $IP | where{$_.Key -match "ntpd"}
				Write-Host -BackgroundColor:Black -ForegroundColor:Yellow "Status: Configuring NTPd on $IP ..."
				$ntpclient | Set-VMHostService -Policy:On -Confirm:$false -ErrorAction:Stop | Out-Null 
				$Logvalue = " Status: Configuring NTPd on $IP ..."
				Add-Content -Path $LogPath -Value $Logvalue
				
				
				#Set Timeout for Tech Support Mode
				Write-Host "`n Setting Timeout for Tech Support Mode...`n"
				Get-VMHost -Name $IP | Set-VMHostAdvancedConfiguration -Name "UserVars.ESXiShellTimeOut" 900 -WarningAction silentlycontinue | Out-Null
				$Logvalue = " Status: Setting Timeout for Tech Support Mode 900 milliseconds "
				Add-Content -Path $LogPath -Value $Logvalue
						
						
				#Set SYSLOG Settings
				Write-Host "`n Setting SYSLOG settings for default size and rotate. `n"
				Get-VMHost -Name $IP | Set-VMHostAdvancedConfiguration -Name "Syslog.global.defaultSize" 5120 -WarningAction silentlycontinue | Out-Null
				Get-VMHost -Name $IP | Set-VMHostAdvancedConfiguration -Name "Syslog.global.defaultRotate" 20 -WarningAction silentlycontinue | Out-Null
				$Logvalue = " Setting SYSLOG settings for default size and rotate."
				Add-Content -Path $LogPath -Value $Logvalue
				
				#Add SIM Account
				Write-Host "`n Adding Service Account...`n"
				try { 
						$user = Get-VIAccount -User -Id $global:ServiceAccount -Server $IP  -ErrorAction silentlycontinue; 
						if($user -eq $null){
							New-VMHostAccount -Server $IP -Id $global:ServiceAccount -Password $global:ServiceAccountPassword -Description "Test Account" | Out-Null
							Get-VMHost | New-VIPermission -Principal $global:ServiceAccount -Role Admin | Out-Null
							Write-Host " Added Service Account with admin permission..." -ForegroundColor Green
							$Logvalue = " Added Service Account with admin permission...."
							Add-Content -Path $LogPath -Value $Logvalue
						}else{
						Write-Host " The specified key, name, or identifier '$global:ServiceAccount' already exists...."  -ForegroundColor Yellow
						$Logvalue = " The specified key, name, or identifier '$global:ServiceAccount' already exists...."
						Add-Content -Path $LogPath -Value $Logvalue}
				}
				catch {
						New-VMHostAccount -Server $IP -Id $global:ServiceAccount -Password $global:ServiceAccountPassword -Description "Test Account" | Out-Null
						Get-VMHost | New-VIPermission -Principal $global:ServiceAccount -Role Admin | Out-Null
						Write-Host " Added Service Account with admin permission..." -ForegroundColor Green
						$Logvalue = " Added Service Account with admin permission...."
						Add-Content -Path $LogPath -Value $Logvalue
				}

				#Change root Password
				Write-Host "`n Changing root password...`n"
				Set-VMHostAccount -UserAccount root -password $global:Newrootpassword -Confirm:$false | Out-Null
				$Logvalue = " Changing root password..."
				Add-Content -Path $LogPath -Value $Logvalue
				
				#Set to Maintenance Mode
				Write-Host " Putting ESXi server in Maintenance Mode..."
				Set-VMHost -State Maintenance -VMHost $IP | Out-Null
				$Logvalue = " Putting ESXi server in Maintenance Mode..."
				Add-Content -Path $LogPath -Value $Logvalue

				#Disconnect from ESXi server
				Disconnect-VIServer $IP -confirm:$false
			}
		
		}
		
				Write-Host "*********************************************************************************"
				Write-Host "Script Execution Complete"
				Write-Host "Review Logfile for errors"
				Write-Host "Logfile:" $logfile
		
}

# Main
clear
Main

